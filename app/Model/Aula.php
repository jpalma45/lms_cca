<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    protected $primaryKey = "idaula";

    protected $fillable = [
        'nombre'
    ];
    
    public $timestamps = false;


}
