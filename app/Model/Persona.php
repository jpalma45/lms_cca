<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
        protected $fillable = [
        'nombre','snombre','apellido','cedula',
        'direccion','telefono','fecha_nacimiento',
        'Id_tipopersona','estado','email','especializacion'
    ];

    protected $primaryKey = "idpersona";

    public $timestamps = false;
}
