<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Contenido extends Model
{
    protected $fillable = [
        'titulo','contenido'
    ];

    protected $primaryKey = "idcontenido";

    public $timestamps = false;
}
