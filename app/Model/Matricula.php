<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    protected $fillable = [
        'pago','idpersona','idgrupo'
    ];

    protected $primaryKey = "idmatriculas";

}
