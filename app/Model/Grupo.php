<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $fillable = [
        'nombregrupo','cantidad','aula_idaula','horario','iddocentes'
    ];

    protected $primaryKey = "idgrupo";

    public $timestamps = false;
}
