<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Programacione extends Model
{
    protected $fillable = [
        'nombre','inicio','fin','idcontenido','idpersona','idgrupo'
    ];

    protected $primaryKey = "idprogramacion";

    public $timestamps = false;
}
