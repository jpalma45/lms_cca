<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $fillable = [
        'name','email','rol','password','avatar'
    ];

}
