<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\RequestAula;
use App\Model\Aula;
use Notify;

class AulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aula = Aula::all()->where('estado','=','1');
        $aulaestado = Aula::all()->where('estado','=','0');
        return view('aula.aula',compact('aula','aulaestado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestAula $request)
    {
        $input = $request->all();
        Aula::create($input);
        Notify::success("Registro exitoso");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aula = Aula::findOrFail($request->idaula)->update($request->all());
        Notify::success("actualizados","Datos");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::table('aulas')->where('idaula',$request->idaula)->update(['estado' => 0]);
        Notify::success("Cambio de estado exitoso");
        return redirect()->back();
    }

    public function activar(Request $request)
    {
        DB::table('aulas')->where('idaula',$request->idaula)->update(['estado' => 1]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }
}
