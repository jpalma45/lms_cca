<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\RequestGrupo;
use App\Model\Grupo;
use App\Model\Aula;
use App\Model\Programacione;
use Notify;

class GrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   // $grupo = Grupo::all();

        $grupo = DB::table('grupos')
        ->join('aulas', 'aulas.idaula', '=', 'grupos.aula_idaula')
        ->select('aulas.*', 'grupos.nombregrupo', 'grupos.cantidad','grupos.aula_idaula','grupos.idgrupo',
        'grupos.horario')->where('grupos.estado','=','1')
        ->get();

        $estadogrupo = DB::table('grupos')
        ->join('aulas', 'aulas.idaula', '=', 'grupos.aula_idaula')
        ->select('aulas.*', 'grupos.nombregrupo', 'grupos.cantidad','grupos.aula_idaula','grupos.idgrupo',
        'grupos.horario')->where('grupos.estado','=','0')
        ->get();

        $aula = Aula::all()->where('estado','=','1');
        $programacion = Programacione::all();
        return view('grupo.grupo',compact('grupo','aula','estadogrupo','programacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestGrupo $request)
    {
        $estado = DB::table('aulas')->select('estado')->where('idaula',$request->aula_idaula)->get();
        

        if($estado=1){
        $input = $request->all();
        Grupo::create($input);
        Notify::success("Registro exitoso");
        $aula = DB::table('aulas')->where('idaula',$request->aula_idaula)->update(['estado' => 0]);
        return redirect()->back();
       }else{
        Notify::error("esta en uso","El aula");
        return redirect()->back();
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $personas = DB::table('personas')->select('*') ->where([ ['Id_tipopersona','=','2'], ['estado','=','1'],])->get();
        
        $grupos = Grupo::all();

        $gruposhow = Grupo::all()->where('idgrupo',$id)->first();

        $programacion = DB::table('programaciones')
        ->join('contenidos','contenidos.idcontenido', '=', 'programaciones.idcontenido')
        ->select('programaciones.*','contenidos.*')->where('programaciones.idgrupo',$id)->get();

        $matriculagrupos = DB::table('matriculas')
        ->join('personas', 'personas.idpersona', '=', 'matriculas.idpersona')
        ->select('matriculas.*', 'personas.nombre', 'personas.cedula','personas.direccion','personas.telefono',
        'personas.fecha_nacimiento','personas.apellido','personas.snombre')->where([ ['idgrupo','=',$id], ['matriculas.estado','=','1'],])
        ->get();

        $estadoM = DB::table('matriculas')
        ->join('personas', 'personas.idpersona', '=', 'matriculas.idpersona')
        ->select('matriculas.*', 'personas.nombre', 'personas.cedula','personas.direccion','personas.telefono',
        'personas.fecha_nacimiento','personas.apellido','personas.snombre')->where([ ['idgrupo','=',$id], ['matriculas.estado','=','0'],])
        ->get();


        return view('grupo.consultaGrupo',compact('gruposhow','matriculagrupos','grupos','personas','estadoM','programacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grupo = Grupo::findOrFail($request->idgrupo)->update($request->all());
        Notify::success("actualizados","Datos");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $aula = DB::table('grupos')->where('idgrupo',$request->idgrupo)->update(['estado' => 0]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }

    public function activar(Request $request)
    {
        $aula = DB::table('grupos')->where('idgrupo',$request->idgrupo)->update(['estado' => 1]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }
}
