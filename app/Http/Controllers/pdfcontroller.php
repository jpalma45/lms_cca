<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;

class pdfcontroller extends Controller
{
    public function viewpdf()
    {
        return view('reporte.listareportes');
    }

    public function acercade()
    {
        return view('reporte.acercade');
    }

    public function mapanavegacion()
    {
        return  view('reporte.mapadenavegacion');
    }

    public function ayuda()
    {
        return view('reporte.ayuda');
    }

    public function  reportepdf()
    {
        $matriculas = DB::table('matriculas')
        ->join('personas', 'personas.idpersona', '=', 'matriculas.idpersona')
        ->select('matriculas.*', 'personas.nombre', 'personas.cedula','personas.direccion','personas.telefono',
        'personas.fecha_nacimiento','personas.apellido','personas.snombre')->get();

        $cont = DB::table('matriculas')->count();

        $pdf = PDF::loadview('reporte.pdf', ['matriculas' => $matriculas, 'cont' => $cont]);
        return $pdf->download('reporte.pdf');
    }

    public function reportemesanterior()
    {
        $firstDayofPreviousMonth = Carbon::now()->startOfMonth()->subMonth()->toDateString(); 
        $lastDayofPreviousMonth = Carbon::now()->endOfMonth()->subMonth()->toDateString();

        $matriculas = DB::table('matriculas')
        ->join('personas', 'personas.idpersona', '=', 'matriculas.idpersona')
        ->select('matriculas.*', 'personas.nombre', 'personas.cedula','personas.direccion','personas.telefono',
        'personas.fecha_nacimiento','personas.apellido','personas.snombre')->whereBetween('matriculas.created_at',[$firstDayofPreviousMonth, $lastDayofPreviousMonth])->get(); 
    
        $cont = DB::table('matriculas')->whereBetween('matriculas.created_at',[$firstDayofPreviousMonth, $lastDayofPreviousMonth])->count();

        $pdf = PDF::loadview('reporte.mesanterior', ['matriculas' => $matriculas, 'cont' => $cont]);
        return $pdf->download('reporte_mes_anterior.pdf');
    }

    public function reportemesactual()
    {
        $primerdia = Carbon::now();
        $primerdia->modify('first day of this month');
        $primerdia->format('d/m/Y');

        $ultimodia = Carbon::now();
        $ultimodia->modify('last day of this month');
        $ultimodia->format('d/m/Y');

        $matriculas = DB::table('matriculas')
        ->join('personas', 'personas.idpersona', '=', 'matriculas.idpersona')
        ->select('matriculas.*', 'personas.nombre', 'personas.cedula','personas.direccion','personas.telefono',
        'personas.fecha_nacimiento','personas.apellido','personas.snombre')->whereBetween('matriculas.created_at',[$primerdia, $ultimodia])->get(); 
    
        $cont = DB::table('matriculas')->whereBetween('matriculas.created_at',[$primerdia, $ultimodia])->count();

        $pdf = PDF::loadview('reporte.reporte_mes_actual', ['matriculas' => $matriculas, 'cont' => $cont]);
        return $pdf->download('reporte_mes_actual.pdf');

    }
}
