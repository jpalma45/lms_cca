<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPersona;
use App\Model\Tipo_persona;
use App\Model\Persona;
use Carbon\Carbon;
use Notify;
use Datatables;

class PersonaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $date = Carbon::now();
        $date = $date->format('Y-m-d');
        $personas = DB::table('personas')->select('*') ->where([ ['Id_tipopersona','=','2'], ['estado','=','1'],])->get();
        $docentes = DB::table('personas')->select('*') ->where([ ['Id_tipopersona','=','1'], ['estado','=','1'],])->get();

        $tipo_persona = Tipo_persona::all();
        $estado = Persona::all()->where('estado','=','0');
        return view('persona.persona',compact('tipo_persona','personas','estado','date','docentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function generatePDF()

    {

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->download();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestPersona $request)
    {
        
        $date = Carbon::now();
 
        $date = $date->format('Y-m-d');
        //dd($date);
        if($request->fecha_nacimiento<$date){
        $input = $request->all();
        Persona::create($input);
        Notify::success("Registro exitoso");
        return back();
        }else{
        Notify::error('No puede ser mayor ala actual', 'La Fecha de nacimiento');
        return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activar(Request $request)
    {
        DB::table('personas')->where('idpersona',$request->idpersona)->update(['estado' => 1]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function desactivar(Request $request)
    {
        $personas = Persona::findOrFail($request->idpersona);
        $personas->estado = '0';
        $personas->save();
        Notify::success("Cambio de estado exitoso");
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $persona = Persona::findOrFail($request->idpersona)->update($request->all());
        Notify::success("actualizados","Datos");
        return back();
        /*dd($request->all());*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        DB::table('personas')->where('idpersona',$request->idpersona)->update(['estado' => 0]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }
}
