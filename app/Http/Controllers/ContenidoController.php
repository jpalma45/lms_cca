<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\ContenidoRequest;
use App\Model\Contenido;
use Carbon\Carbon;
use Notify;
use Datatables;

class ContenidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contenido = Contenido::all()->where('estado','=','1');
        $estado = Contenido::all()->where('estado','=','0');
        return view('contenido.contenido',compact('contenido','estado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContenidoRequest $request)
    {
        $input = $request->all();
        Contenido::create($input);
        Notify::success("Registro exitoso");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $persona = Contenido::findOrFail($request->idcontenido)->update($request->all());
        Notify::success("actualizados","Datos");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        DB::table('contenidos')->where('idcontenido',$request->idcontenido)->update(['estado' => 0]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }

    public function activar(Request $request)
    {
        DB::table('contenidos')->where('idcontenido',$request->idcontenido)->update(['estado' => 1]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }
}
