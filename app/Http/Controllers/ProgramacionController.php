<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\ProgramacionRequest;
use App\Model\Programacione;
use App\Model\Contenido;
use App\Model\Persona;
use App\Model\Grupo;
use Carbon\Carbon;
use Notify;
use Datatables;

class ProgramacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $persona = Persona::all()->where('Id_tipopersona','=','1');
       $contenido = Contenido::all()->where('estado','=','1');
       $grupo = Grupo::all()->where('estado','=','1');
       $programacion = Programacione::all()->where('estado','=','1');
       $estado = Programacione::all()->where('estado','=','0');
       return view('programacion.horario',compact('programacion','estado','contenido','persona','grupo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramacionRequest $request)
    {
        $input = $request->all();
        Programacione::create($input);
        Notify::success("Registro exitoso");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $programacion = Programacione::findOrFail($request->idprogramacion)->update($request->all());
        Notify::success("actualizados","Datos");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::table('programaciones')->where('idprogramacion',$request->idprogramacion)->update(['estado' => 0]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }

    public function activar(Request $request)
    {
        DB::table('programaciones')->where('idprogramacion',$request->idprogramacion)->update(['estado' => 1]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }
}
