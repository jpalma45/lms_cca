<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\RequestUsuario;
use App\Http\Requests\updateUserRequest;
use App\Model\User;
use Notify;
use Datatables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = User::where([ ['estado','=','1'], ['id','!=', auth()->id()],])->get();
        $estado = User::where([ ['estado','=','0'], ['id','!=', auth()->id()],])->get();
        return view('usuario.usuario', compact('usuario','estado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'rol' => 'required|string|email|max:6|unique:users'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestUsuario $request)
    {
        $data = $request->all();
        
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'rol' => $data['rol']
        ]);

        Notify::success("Registro exitoso");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);

       // $this->authorize($user);
       if($request->hasFile('avatar')){

        $user->avatar = $request->file('avatar')->store('public');
        
       }
       if($request->input('password')){

        $user->password = bcrypt($request['password']);

       }
       if($request->input('email')){

           $user->email = $request->input('email');
       }
        $user->update($request->only('name'));
        //$user->roles()->sync($request->roles);
        Notify::success("actualizados","Datos");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //<span>{{ auth()->user()->name }}</span>
        if(auth()->user()->id<>$id){
            DB::table('users')->where('id',$id)->update(['estado' => 0]);
            Notify::success("Cambio de estado exitoso");
            return back();
        }else{
            Notify::error('No puede ser desactivado con su sesion en inicio', 'El Usuario');
            return redirect()->back();   
        }
        
    }

    public function activar(Request $request)
    {
        DB::table('users')->where('id',$request->iduser)->update(['estado' => 1]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }
}
