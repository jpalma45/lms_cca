<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $grupo=DB::table('grupos')->count();
        $usuario = DB::table('users')->select('estado')->where('id','=', auth()->id())->get();
        $personas=DB::table('personas')->count();
        $matriculas=DB::table('matriculas')->count();
        $usuarioR=$usuario[0]->estado;

        return view('index.IndexContent',compact('grupo','personas','matriculas'));
    }

}
