<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\RequestMatricula;
use App\Model\Grupo;
use App\Model\Persona;
use App\Model\Matricula;
use Notify;
use Datatables;

class MatriculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { // $personas = Persona::all()->where('Id_tipopersona','=','1','estado','=','1');
      // $matriculas = Matricula::all();
      //select count(*) as cantidad from matriculas group by idpersona

     // $cont= Matricula::all();
     // dd($cont[0]->idpersona);


    $personas = DB::table('personas')->select('*') ->where([ ['Id_tipopersona','=','2'], ['estado','=','1'],])->get();

    $matriculas = DB::table('matriculas')
    ->join('personas', 'personas.idpersona', '=', 'matriculas.idpersona')
    ->select('matriculas.*', 'personas.nombre', 'personas.cedula','personas.direccion','personas.telefono',
    'personas.fecha_nacimiento','personas.apellido','personas.snombre')->where('matriculas.estado','=','1')
    ->get();


    $estadoM = DB::table('matriculas')
    ->join('personas', 'personas.idpersona', '=', 'matriculas.idpersona')
    ->select('matriculas.*', 'personas.nombre', 'personas.cedula','personas.direccion','personas.telefono',
    'personas.fecha_nacimiento','personas.apellido','personas.snombre')->where('matriculas.estado','=','0')
    ->get();
    
        $grupos = Grupo::all();
        return view('matricula.matricula',compact('personas','grupos','matriculas','estadoM'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestMatricula $request)
    {

        $cont=DB::table('matriculas')->where('idgrupo',$request->idgrupo)->count();


        if($cont <= 6){
         foreach($request['idpersona'] as $key=> $value){
            Matricula::create(['pago'=>$request['pago'],
            'idpersona'=>$request['idpersona'][$key],
            'idgrupo'=>$request['idgrupo']
            ]);
         }   
/**
     *   $input = $request->all();
     *   Matricula::create($input);
        */
        Notify::success("Registro exitoso");
        DB::table('personas')->where('idpersona',$request->idpersona)->update(['estado' => 0]);
        return redirect()->back();
        }else{
        Notify::error('El limite de matriculas por grupo es de 7 estudiantes', 'Exedio el numero de matriculas');
        return redirect()->back();   
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $detalles = DB::table('matriculas')
        ->join('personas', 'personas.idpersona','=','matriculas.idpersona')
        ->join('grupos','matriculas.idgrupo','=','grupos.idgrupo')
        ->select('personas.nombre','personas.apellido','personas.cedula','personas.direccion','personas.telefono',
        'personas.fecha_nacimiento','personas.email','personas.snombre','grupos.nombregrupo','grupos.horario',
        'matriculas.pago')
        ->where('matriculas.idmatriculas','=',$id)->get();

        return view('matricula.consultamatricula',compact('detalles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $matriculas = Matricula::findOrFail($request->idmatriculas)->update($request->all());
        Notify::success("actualizados","Datos");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::table('matriculas')->where('idmatriculas',$request->idmatricula)->update(['estado' => 0]);
        Notify::success("Cambio de estado exitoso");
        return redirect()->back();
    }


    public function activar(Request $request)
    {
        DB::table('matriculas')->where('idmatriculas',$request->idmatricula)->update(['estado' => 1]);
        Notify::success("Cambio de estado exitoso");
        return back();
    }
}
