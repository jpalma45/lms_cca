<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPersona extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:8',
            'apellido' => 'required|string|max:20',
            'cedula' => 'required|string|max:14|min:10|unique:personas',
            'telefono' => 'required|string|max:14|min:9',
            'direccion' => 'required|string',
            'fecha_nacimiento' => 'required|date',
            'Id_tipopersona' => 'required',
        ];
    }
}
