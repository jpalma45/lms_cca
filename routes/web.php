<?php

/*Route::get('text',function(){
$user = new App\User;
$user->name = 'david';
$user->email = 'david@gmail.com';
$user->rol = 'docente';
$user->password = bcrypt('12345678');
$user->save();

return $user;
});*/

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Controller personalizazados */
Route::get('/',['as' => '/', 'uses' => 'HomeController@index'])->middleware('auth');


Route::resource('persona', 'PersonaController');

Route::resource('matricula', 'MatriculaController');

Route::resource('users', 'UserController');

Route::resource('aula', 'AulaController');

Route::resource('grupo', 'GrupoController');

Route::resource('programacion','ProgramacionController');

Route::resource('contenido','ContenidoController');

Route::resource('perfil', 'PerfilController');

Route::get('pdf/reporte', 'pdfcontroller@reportepdf')->name('reporte.pdf');
Route::get('pdf/reportemes', 'pdfcontroller@reportemesanterior')->name('reportemes.pdf');
Route::get('pdf/reporteactual', 'pdfcontroller@reportemesactual')->name('reporteactual.pdf');
Route::get('view/reporte', 'pdfcontroller@viewpdf')->name('vista.pdf');
Route::get('view/acercade', 'pdfcontroller@acercade')->name('vista.acercade');
Route::get('view/mapa', 'pdfcontroller@mapanavegacion')->name('vista.mapanav');
Route::get('view/ayuda', 'pdfcontroller@ayuda')->name('vista.ayuda');




Route::post('persona/activar', 'PersonaController@activar')->name('persona.activar');
Route::post('users/activar', 'UserController@activar')->name('users.activar');
Route::post('aula/activar', 'AulaController@activar')->name('aula.activar');
Route::post('grupo/activar', 'GrupoController@activar')->name('grupo.activar');
Route::post('matricula/activar', 'MatriculaController@activar')->name('matricula.activar');
Route::post('programacion/activar', 'ProgramacionController@activar')->name('programacion.activar');
Route::post('contenido/activar', 'ContenidoController@activar')->name('contenido.activar');


/*End personalizazados */

/*Login y passwor reset */

Route::get('login', 'Auth\LoginController@showLoginForm');

Route::post('login', 'Auth\LoginController@login');

Route::get('logout', 'Auth\LoginController@logout');

Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm');

Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::get('password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');

Route::post('/password/reset','ResetPasswordController@reset')->name('password.send');

Auth::routes();
/*Fin login y Pssword reset */

Route::resource('home','HomeController');

