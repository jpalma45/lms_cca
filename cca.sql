-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-12-2018 a las 20:14:00
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulas`
--

CREATE TABLE `aulas` (
  `idaula` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `estado` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aulas`
--

INSERT INTO `aulas` (`idaula`, `nombre`, `estado`) VALUES
(1, 'AB-1', 0),
(2, 'A25', 1),
(3, 'AB3', 0),
(4, 'BN5', 0),
(5, 'JP4', 0),
(6, '706', 0),
(7, 'tarde', 1),
(8, 'ingles-4', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenidos`
--

CREATE TABLE `contenidos` (
  `idcontenido` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `contenido` varchar(100) DEFAULT NULL,
  `estado` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contenidos`
--

INSERT INTO `contenidos` (`idcontenido`, `titulo`, `contenido`, `estado`) VALUES
(3, 'ingles', 'dsdsdsd', 1),
(4, 'ingles', 'ingles', 1),
(5, 'scrum', 'metodologias agiles', 0),
(6, 'iso89', 'php', 1),
(7, 'iso78', 'validaciones', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `idgrupo` int(11) NOT NULL,
  `nombregrupo` varchar(20) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` int(1) NOT NULL DEFAULT '1',
  `aula_idaula` int(11) NOT NULL,
  `horario` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`idgrupo`, `nombregrupo`, `cantidad`, `estado`, `aula_idaula`, `horario`) VALUES
(6, 'expo 2', 7, 1, 3, 'Mañana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas`
--

CREATE TABLE `matriculas` (
  `idpersona` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `pago` varchar(45) NOT NULL,
  `estado` int(1) NOT NULL DEFAULT '1',
  `idmatriculas` int(10) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `matriculas`
--

INSERT INTO `matriculas` (`idpersona`, `idgrupo`, `pago`, `estado`, `idmatriculas`, `created_at`, `updated_at`) VALUES
(11, 6, '888.888', 1, 1, '2018-12-07', '2018-12-08 02:53:42'),
(12, 6, '888.888', 1, 2, '2018-12-07', '2018-12-08 02:53:42'),
(13, 6, '888.888', 1, 3, '2018-12-07', '2018-12-08 02:53:42'),
(14, 6, '888.888', 1, 4, '2018-12-07', '2018-12-08 02:53:42'),
(15, 6, '888.888', 1, 5, '2018-12-07', '2018-12-08 02:53:42'),
(16, 6, '888.888', 1, 6, '2018-12-07', '2018-12-08 02:53:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_11_04_092633_add_avatar_to_users_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jpalma45@gmail.com', '$2y$10$li1c78AgCk5Yl2vX2bkPi.Z3tey5I.VpZhWaTPu6f2A5/YM7PBqfS', '2018-10-31 03:40:45'),
('arango829@hotmail.com', '$2y$10$7AwxF93QyR3K15bPY4Ml6u6cBiv83WrCr4I7Nw8aPQAMDX2jU154m', '2018-11-04 01:47:23'),
('jpalma45@misena.edu.co', '$2y$10$KzEGfJc1sUQEs0UVsWokfeEUiQpIkth6eea3vF74GA5QMTCrwNYgO', '2018-11-19 03:03:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(10) NOT NULL,
  `cedula` varchar(14) NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` varchar(14) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `Id_tipopersona` int(11) NOT NULL DEFAULT '1',
  `apellido` varchar(20) NOT NULL,
  `snombre` varchar(20) DEFAULT '.',
  `estado` int(1) NOT NULL DEFAULT '1',
  `email` varchar(60) NOT NULL,
  `especializacion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`idpersona`, `nombre`, `cedula`, `direccion`, `telefono`, `fecha_nacimiento`, `Id_tipopersona`, `apellido`, `snombre`, `estado`, `email`, `especializacion`) VALUES
(1, 'johan', '1079291154', 'miramar', '3133196812', '2018-10-26', 2, 'perea', '.', 0, '', NULL),
(2, 'juan', '1021232323', 'bello', '232323233232', '2018-10-26', 1, 'palacios', '.', 1, '', NULL),
(3, 'juan', '3.443.434.384', 'bello', '4.343.434.343', '2018-10-01', 1, 'palacios', 'david', 0, '', NULL),
(4, 'juan', '20123131321', 'bello', '3145816032', '2018-10-01', 1, 'palacios', 'david', 1, '', NULL),
(5, 'hector', '1.067.890.400', 'cra 82', '4.100.000.000', '2018-10-30', 1, 'maya ramirez', 'dario', 0, '', NULL),
(6, 'johan', '4.304.930.493', 'bello', '3.133.196.812', '2018-11-01', 2, 'palma', '.', 0, 'karen@gmail.com', NULL),
(7, 'jorge', '3.232.323.232', 'miramar', '3.232.323.232', '2018-10-30', 1, 'palacios', '.', 0, 'jorge@gmail.com', 'español'),
(8, 'pepito', '8.983.928.392', 'rio', '3.434.343.434', '2018-11-01', 2, 'palacio', '.', 0, 'pepito68@gmail.com', NULL),
(9, 'pepito2', '7.374.734.374', 'rio', '2.323.232.323', '2018-11-01', 2, 'palacio', '.', 0, 'pepito89@gmail.com', NULL),
(10, 'hector', '1.067.890.000', 'cr', '3.109.323.323', '2018-11-02', 2, 'mayas', 'dario', 0, 'hectormaya@gmail.com', NULL),
(11, 'camila', '9.323.232.323', 'bello', '4.343.434.343', '2018-10-29', 2, 'gomez', '.', 0, 'camila@gmail.com', NULL),
(12, 'manuel', '1.732.833.423', 'bello', '3.323.232.323', '2018-11-06', 2, 'alvarez', 'alejandro', 1, 'manuel@gmail.com', NULL),
(13, 'johan', '4.382.828.283', 'miramar', '8.327.382.732', '2018-10-29', 2, 'palma', '.', 1, 'johanpalma@gmail.com', NULL),
(14, 'deimar', '7.863.535.477', 'miramar', '3.433.434.334', '2018-10-28', 2, 'palma', 'arley', 1, 'deimar@gmail.com', NULL),
(15, 'paola', '7.948.383.883', 'bello', '3.093.382.743', '2018-11-07', 2, 'moquera', '.', 1, 'paola68@gmail.com', NULL),
(16, 'arley', '1.084.638.937', 'miramar', '3.133.196.812', '2018-11-04', 2, 'palma', '.', 1, 'arleypalma@gmail.com', NULL),
(17, 'deiner', '3.323.232.323', 'bello', '3.232.322.323', '2018-11-07', 2, 'machado', NULL, 1, 'deiner@gmail.com', NULL),
(18, 'diana', '6.565.656.565', 'robledo', '4.343.343.434', '2018-11-15', 2, 'palma', 'marcela', 1, 'marcela@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programaciones`
--

CREATE TABLE `programaciones` (
  `idprogramacion` int(11) NOT NULL,
  `inicio` date NOT NULL,
  `fin` date NOT NULL,
  `estado` int(1) NOT NULL DEFAULT '1',
  `nombre` varchar(45) NOT NULL,
  `idcontenido` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `programaciones`
--

INSERT INTO `programaciones` (`idprogramacion`, `inicio`, `fin`, `estado`, `nombre`, `idcontenido`, `idpersona`, `idgrupo`) VALUES
(15, '2018-12-03', '2018-12-14', 1, 'prueba-1', 6, 4, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_personas`
--

CREATE TABLE `tipo_personas` (
  `Id_tipopersona` int(11) NOT NULL,
  `tipo_persona` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_personas`
--

INSERT INTO `tipo_personas` (`Id_tipopersona`, `tipo_persona`) VALUES
(1, 'docente'),
(2, 'estudiante ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png',
  `password` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `rol` varchar(20) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'estudiante',
  `estado` int(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `avatar`, `password`, `rol`, `estado`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'johan', 'jpalma45@gmail.com', 'public/az8i5AEZorWpWltlwAjAhUaLGTrV9HQTY01r6MZy.jpeg', '$2y$10$2CGTL4o4Rosl32IxoQkSmOHP1Q2lmxBKce8j85cowcJ8gNuRUpCf6', 'docente', 1, 'PybnGofXNjAXwTdFSQdRh4TyXZduIbrfcCbG2swxWVyWcn7YdmKFOiD2MU5T', '2018-10-26 09:07:49', '2018-11-11 03:57:27'),
(2, 'johan', 'jpalma45@misena.edu.co', 'public/vL8n7dnVg7NfxUQA2yHo9a4O6KJOU9n3mrAtqZrN.jpeg', '$2y$10$1D1DXeqn84Lhd5gpNr2BsOWlqbfb4bQBJmpSIVOBj1jTLhHO375QK', 'admin', 1, 'Zx6WYd8ZLiaQTpMY1NObHziAWOZGxPgXYHnbSdECovmMb5ymzjhTc6d76Z54', '2018-10-31 06:38:09', '2018-11-24 11:56:22'),
(3, 'johan', 'jpalma46@misena.edu.co', 'public/TGSRXkLxbYoiv65BuzMxtrhis4RSol3WLoyNadTs.jpeg', '$2y$10$zgyvj7eDAfYf2qhPe/46auBhGT/VHyzEZB/mgI5PcsIN8ZET8fSde', 'admin', 0, NULL, '2018-11-04 01:46:26', '2018-11-04 22:23:01'),
(4, 'juanpalma', 'juanpalma@gmail.com', 'user.png', '$2y$10$/oTO6lMfEpVxYXk9W1rkMeIRxpZFz5eooUNfnjjVIPJd37c6tB7Ry', 'docente', 0, NULL, '2018-11-14 12:16:11', '2018-11-14 12:16:11'),
(5, 'pepito', 'pepito@gmail.com', 'user.png', '$2y$10$rz761TrT9uxGkTgFJuocwOFQ.hj/aJPjSKGCrDaa0TvPdImgZV1U2', 'estudiante', 1, NULL, '2018-11-15 08:05:39', '2018-11-15 08:05:39'),
(6, 'keimar', 'keimar@gmail.com', 'user.png', '$2y$10$kFR1bKM5zYLWa7z8RIScj.81vhZAjM0w.sf8vJecgMkyjiV0ojjmG', 'admin', 1, NULL, '2018-11-24 00:33:16', '2018-11-24 00:33:16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aulas`
--
ALTER TABLE `aulas`
  ADD PRIMARY KEY (`idaula`);

--
-- Indices de la tabla `contenidos`
--
ALTER TABLE `contenidos`
  ADD PRIMARY KEY (`idcontenido`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`idgrupo`),
  ADD KEY `fk_grupo_aula1_idx` (`aula_idaula`);

--
-- Indices de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD PRIMARY KEY (`idmatriculas`,`idpersona`,`idgrupo`),
  ADD KEY `fk_personas_has_grupos_grupos1_idx` (`idgrupo`),
  ADD KEY `fk_personas_has_grupos_personas1_idx` (`idpersona`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`idpersona`),
  ADD KEY `fk_persona_tipo_persona_idx` (`Id_tipopersona`);

--
-- Indices de la tabla `programaciones`
--
ALTER TABLE `programaciones`
  ADD PRIMARY KEY (`idprogramacion`),
  ADD KEY `fk_programaciones_contenidos1_idx` (`idcontenido`),
  ADD KEY `fk_programaciones_personas1_idx` (`idpersona`);

--
-- Indices de la tabla `tipo_personas`
--
ALTER TABLE `tipo_personas`
  ADD PRIMARY KEY (`Id_tipopersona`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aulas`
--
ALTER TABLE `aulas`
  MODIFY `idaula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `contenidos`
--
ALTER TABLE `contenidos`
  MODIFY `idcontenido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `idgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  MODIFY `idmatriculas` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `idpersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `programaciones`
--
ALTER TABLE `programaciones`
  MODIFY `idprogramacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tipo_personas`
--
ALTER TABLE `tipo_personas`
  MODIFY `Id_tipopersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD CONSTRAINT `fk_personas_has_grupos_grupos1` FOREIGN KEY (`idgrupo`) REFERENCES `grupos` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_personas_has_grupos_personas1` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `fk_persona_tipo_persona` FOREIGN KEY (`Id_tipopersona`) REFERENCES `tipo_personas` (`Id_tipopersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `programaciones`
--
ALTER TABLE `programaciones`
  ADD CONSTRAINT `fk_programaciones_contenidos1` FOREIGN KEY (`idcontenido`) REFERENCES `contenidos` (`idcontenido`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_programaciones_personas1` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
