$( "#modalpersona" ).validate({
    rules: {
      nombre: {
        required: true,
        maxlength: 20,
        minlength: 3
      },
      snombre: {
        maxlength: 20,
        minlength: 3
      },
      apellido:{
        required: true,
        maxlength: 50,
        minlength: 4
      },
      cedula: {
        required: true,
        minlength: 10,
        maxlength: 14
      },
      telefono: {
        required: true,
        minlength: 9,
        maxlength : 14
      },
      direccion:{
        required: true,
      },
      email:{
        required: true,
        email: true
      },
      fecha_nacimiento:{
        required: true,
        date: true
      },
      Id_tipopersona:{
        required: true
      }
    }
  });

  $( "#editarpersona" ).validate({
    rules: {
      nombre: {
        required: true,
        maxlength: 20,
        minlength: 3
      },
      snombre: {
        maxlength: 20
      },
      apellido:{
        required: true,
        maxlength: 50,
        minlength: 5
      },
      cedula: {
        required: true,
        minlength: 10,
        maxlength: 14
      },
      telefono: {
        required: true,
        minlength: 9,
        maxlength : 14,
      },
      direccion:{
        required: true,
      },
      email:{
        required: true,
        email: true
      },
      fecha_nacimiento:{
        required: true,
        date: true
      },
      Id_tipopersona:{
        required: true
      }
    }
  });

  $( "#modalaula" ).validate({
    rules: {
      nombre: {
        required: true,
        maxlength: 10,
        minlength: 3
      }
    }
  });

  $( "#editaraula" ).validate({
    rules: {
      nombre: {
        required: true,
        maxlength: 10,
        minlength: 3
      }
    }
  });

  $( "#modalcontenido" ).validate({
    rules: {
      titulo: {
        required: true,
        maxlength: 50,
        minlength: 3
      },
      contenido: {
        required: true,
        maxlength: 100,
      }
    }
  });

  $( "#editarcontenido" ).validate({
    rules: {
      titulo: {
        required: true,
        maxlength: 50,
        minlength: 3
      },
      contenido: {
        required: true,
        maxlength: 100,
      }
    }
  });

  $( "#modalgrupo" ).validate({
    rules: {
      nombregrupo: {
        required: true,
        maxlength: 20,
        minlength: 4
      },
      cantidad: {
        required: true,
        range: [4, 7]

      }
    }
  });

  $( "#editargrupo" ).validate({
    rules: {
      nombregrupo: {
        required: true,
        maxlength: 20,
        minlength: 4
      },
      cantidad: {
        required: true,
        range: [4, 7]

      }
    }
  });

  $( "#modalmatricula" ).validate({
    rules: {
      pago: {
        required: true,
      },
      idpersona:{
        required: true,
      }
    }
  });

  $( "#editarmatricula" ).validate({
    rules: {
      pago: {
        required: true,
      },
      idpersona:{
        required: true,
      }
    }
  });

  $( "#programacion" ).validate({
    rules: {
      nombre: {
        required: true,
        maxlength: 8,
        minlength: 4
      },
      inicio: {
        required: true,
        date: true
      },
      fin: {
        required: true,
        date: true
      }
    }
  });

  $( "#editarprogramacion" ).validate({
    rules: {
      nombre: {
        required: true,
        maxlength: 20,
        minlength: 3
      },
      inicio: {
        required: true,
        date: true
      },
      fin: {
        required: true,
        date: true
      }
    }
  });

  $( "#usuario" ).validate({
    rules: {
      name: {
        required: true,
        maxlength: 20,
        minlength: 3
      },
      password: {
        required: true,
      },
      email: {
        required: true,
        email: true
      }
    }
  });

  $( "#editarusuario" ).validate({
    rules: {
      name: {
        required: true,
        maxlength: 20,
        minlength: 4
      },
      email: {
        required: true,
        email: true
      }
    }
  });