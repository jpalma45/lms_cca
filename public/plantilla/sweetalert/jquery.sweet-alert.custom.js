
!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        
    //Basic
    $('#sa-basic').click(function(){
        swal("Here's a message!");
    });

    //A title with a text under
    $('#sa-title').click(function(){
        swal("Here's a message!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.")
    });

    //Success Message
    $('#sa-success').click(function(){
        swal("Good job!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.", "success")
    });

    //Warning Message
    $('.sweetalert').click(function(){
        swal({   
            title: "Esta seguro que desea inhabilitar este dato?",   
            text: "si este dato es inhabilitado no podra ser usado!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",  
            confirmButtonText: "Inhabitar, Si!",   
            cancelButtonText: "Cancelar, No!",  
            closeOnConfirm: false, 
            reverseButtons: true
        }, function(){  
            document.getElementById("int").submit(); 
            swal("Inactivo!", "Acabas de Inhabitar este dato.", "success");    
        });
    });


    //Parameter
    $('sweetalert').click(function(IDob){
        swal({   
            title: "Estas seguro?",   
            text: "Si desactivas este dato no podra ser utilizado!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Inhabitar, Si!",   
            cancelButtonText: "Cancelar, No!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {   
                document.getElementById("").submit();  
                swal("Inactivo!", "Acabas de Inhabitar este dato.", "success");   
            } else {     
                swal("Operacion cancelada", "Sus datos estan seguros", "error");   
            } 
        });
    });

    //Custom Image
    $('#sa-image').click(function(){
        swal({   
            title: "Govinda!",   
            text: "Recently joined twitter",   
            imageUrl: "../../images/avatar.png" 
        });
    });

    //Auto Close Timer
    $('#sa-close').click(function(){
         swal({   
            title: "Auto close alert!",   
            text: "I will close in 2 seconds.",   
            timer: 2000,   
            showConfirmButton: false 
        });
    });


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);