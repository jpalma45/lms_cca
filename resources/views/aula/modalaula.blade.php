<!--Modal Actualizar-->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="box-title text-info" id="exampleModalCenterTitle"><i class="ti-user mr-15"></i>Actualizar Informacion</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form class="form" id="editaraula" action="{{ route('aula.update','text') }}" method="POST">
              {{ method_field('patch') }}
              {{ csrf_field() }}
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                  <input type="hidden" name="idaula" id="aula" value="">
                  <label>Primer nombre <span class="text-danger">*</span></label>
                  <input name="nombre" id="nombre" type="text" class="form-control" placeholder="Nombre Aula">
                  {!!$errors->first('nombre','<span class=error>:message</span>')!!}
                </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="modal-footer modal-footer-uniform">
                <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
                <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Actualizar</button>
            </div> 
          </form>
          <!-- /.box -->
      </div>
    </div>
  </div>
</div>


<!--Agregar-->
<div data-backdrop="static" data-keyboard="false" class="modal center-modal fade" id="modal-center" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Informacion Aula</h4>
              <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <div class="box">
                <!-- /.box-header -->
                
                <form class="form" id="modalaula" action="{{ route('aula.store') }}" method="POST">
                    {{ csrf_field() }}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                        <label>Nombre aula <span class="text-danger">*</span></label>
                        <input name="nombre" id="" type="text" class="form-control" placeholder="Nombre Aula">
                        {!!$errors->first('nombre','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="modal-footer modal-footer-uniform">
                      <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
                      <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Guardar</button>
                  </div> 
                </form>
                <!-- /.box -->			
          </div>
        </div>
      </div>