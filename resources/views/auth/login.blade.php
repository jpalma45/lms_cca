<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../images/favicon.ico">

		<title>Login CCA</title>
		
	<link rel="stylesheet" href="css/toastr.min.css">

	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="plantilla/bootstrap/dist/css/bootstrap.min.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="css/bootstrap-extend.css">
	
	<!-- toast CSS -->
	<link href="plantilla/jquery-toast-plugin-master/src/jquery.toast.css" rel="stylesheet">


	<!-- Theme style -->
	<link rel="stylesheet" href="css/master_style.css">

	<!-- UltimatePro Admin skins -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">	


</head>
<body class="hold-transition bg-img" style="background-image: url(images/auth-bg/banner.jpg);" data-overlay="1">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">	
			
			<div class="col-12">
				<div class="row no-gutters">
					<div class="col-lg-4 col-md-5 col-12">
						<div class="content-top-agile p-10">
							<h2>LMS CCA</h2>
							<p class="text-white">Casa Cultural Andina</p>							
						</div>
						
						<div class="p-30 content-bottom rounded bg-img box-shadowed" style="background-image: url(images/auth-bg/bg.jpg);" data-overlay="8">
							<form method="POST" id="login" action="{{ route('login') }}">
								{{ csrf_field() }}
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text bg-transparent bt-0 bl-0 br-0 no-radius text-white"><i class="ti-user"></i></span>
										</div>
                      <input type="email" name="email" value="{{ old('email') }}" class="form-control pl-15 bg-transparent bt-0 bl-0 br-0 text-white{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email">
									</div>
								</div>
								@if ($errors->has('email'))
								<span class="help-block tst4">
										<strong class="tst4" >{{ $errors->first('email') }}</strong>
								</span>
								@endif
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-transparent bt-0 bl-0 br-0 text-white"><i class="ti-lock"></i></span>
										</div>
                        <input type="password" name="password" class="form-control pl-15 bg-transparent bt-0 bl-0 br-0 text-white {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Contraseña">												
									</div>
								</div>
								@if ($errors->has('password'))
								<span class="help-block tst4">
										<strong class="tst4">{{ $errors->first('password') }}</strong>
								</span>
								@endif
								  <div class="row">
									<div class="col-6">
									  <div class="checkbox text-white">
										<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} >
										<label for="remember">{{ __('Recordar contraseña') }}</label>
									  </div>
									</div>
									<!-- /.col -->
									<div class="col-6">
									 <div class="fog-pwd text-right">
										<a href="{{ route('password.request') }}" class="text-white hover-info"><i class="ion ion-locked"></i> Recuperar Contraseña?</a><br>
									  </div>
									</div>
									<!-- /.col -->
									<div class="col-12 text-center">
										<button type="submit" class="btn btn-info btn-block margin-top-10">Iniciar Sesión</button>
									</div>
									<!-- /.col -->
								  </div>
							</form>	
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery 3 -->
	<script src="plantilla/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="plantilla/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="plantilla/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="js/pages/toastr_1.min.js"></script>


	@if($errors->first('password'))
	<script>
		toastr.error('La Contraseña, Es erronea')
	</script>
	@endif
	@if($errors->first('email'))
	<script>
		toastr.error('El Email, No es correcto por favor verifique su email!')
	</script>
	@endif

</body>

</html>
