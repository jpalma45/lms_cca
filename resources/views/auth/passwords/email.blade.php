<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://ultimatepro-admin-templates.multipurposethemes.com/images/favicon.ico">

    <title>Recuperar contraseña</title>
  
	<link rel="stylesheet" href="../../css/toastr.min.css">

	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../plantilla/bootstrap/dist/css/bootstrap.min.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../css/bootstrap-extend.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="../../css/master_style.css">

	<!-- UltimatePro Admin skins -->
	<link rel="stylesheet" href="../../css/skins/_all-skins.css">	


</head>

<body class="hold-transition bg-img" style="background-image: url(../../images/auth-bg/banner.jpg);" data-overlay="1">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">
			
			<div class="col-12">
				<div class="row no-gutters">
					<div class="col-lg-4 col-md-5 col-12">
						<div class="content-top-agile p-10">
							<h3 class="text-white mb-0">Recuperar Contraseña</h3>								
						</div>
						<div class="p-30 content-bottom rounded bg-img box-shadowed" style="background-image: url(images/auth-bg/bg.jpg);" data-overlay="8">
							<form action="{{ route('password.email') }}" method="post">
								{{ csrf_field() }}
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-transparent bt-0 bl-0 br-0 text-white"><i class="ti-email"></i></span>
										</div>
										<input type="email" name="email" id="email" class="form-control pl-15 bg-transparent bt-0 bl-0 br-0 text-white" placeholder="Correo Electronico">
									</div>
								</div>
								@if ($errors->has('email'))
								<span class="help-block tst4">
										<strong class="tst4" >{{ $errors->first('email') }}</strong>
								</span>
								@endif
								  <div class="row">
									<div class="col-12 text-center">
									  <button type="submit" class="btn btn-info btn-block margin-top-10">Recuperar contraseña</button>
									  <br>
									</div>
									<a href="/"> <span style="color:red"><i class="fa fa-reply"></i>&nbsp;Regresar ?</span> </a>
									<!-- /.col -->
								  </div>
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	



	<!-- jQuery 3 -->
	<script src="../../plantilla/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../../plantilla/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../plantilla/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../../js/pages/toastr_1.min.js"></script>

	@if($errors->first('email'))
	<script>
		toastr.error('No, Pudo procesar el reestablecimeto de contraseña!')
	</script>
	@endif
	@if(session()->has('status'))
	<script>
		toastr.success('Se acaba de ser, Enviado un email de restablecimiento de contraseña!')
	</script>
	@endif

</body>

</html>
