<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://ultimatepro-admin-templates.multipurposethemes.com/images/favicon.ico">

    <title>Recuperar contraseña</title>
  
	<link rel="stylesheet" href="../../../css/toastr.min.css">

	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../../plantilla/bootstrap/dist/css/bootstrap.min.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../../css/bootstrap-extend.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="../../../css/master_style.css">

	<!-- UltimatePro Admin skins -->
	<link rel="stylesheet" href="../../../css/skins/_all-skins.css">	


</head>

<body class="hold-transition bg-img" style="background-image: url(../../../images/auth-bg/banner.jpg);" data-overlay="1">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">
			
			<div class="col-12">
				<div class="row no-gutters">
					<div class="col-lg-4 col-md-5 col-12">
						<div class="content-top-agile p-10">
							<h3 class="text-white mb-0">Recuperar Contraseña</h3>								
						</div>
						<div class="p-30 content-bottom rounded bg-img box-shadowed" style="background-image: url(images/auth-bg/bg.jpg);" data-overlay="8">
							<form action="/password/reset" id="resetpass" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="token" value="{{ $token }}">
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-transparent bt-0 bl-0 br-0 text-white"><i class="ti-email"></i></span>
										</div>
										<input type="email" name="email" id="email" value="{{ $email }}" class="form-control pl-15 bg-transparent bt-0 bl-0 br-0 text-white" placeholder="Email">
									</div>
                                </div>
                                <div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-transparent bt-0 bl-0 br-0 text-white"><i class="ti-email"></i></span>
										</div>
										<input type="password" name="password" id="password" class="form-control pl-15 bg-transparent bt-0 bl-0 br-0 text-white" placeholder="Contraseña">
									</div>
                                </div>
                                <div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-transparent bt-0 bl-0 br-0 text-white"><i class="ti-email"></i></span>
										</div>
										<input type="password" name="password_confirmation" id="password_confirmation" class="form-control pl-15 bg-transparent bt-0 bl-0 br-0 text-white" placeholder="Confirmar Contraseña">
									</div>
								</div>
								  <div class="row">
									<div class="col-12 text-center">
									  <button type="submit" class="btn btn-info btn-block margin-top-10">Recuperar contraseña</button>
									</div>
									<!-- /.col -->
								  </div>
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	



	<!-- jQuery 3 -->
	<script src="../../../plantilla/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../../../plantilla/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../../plantilla/bootstrap/dist/js/bootstrap.min.js"></script>

	<script src="../../../js/pages/toastr_1.min.js"></script>

	@if($errors->any('password'))
	<script>
		toastr.error('La contraseña, No es valida!')
	</script>
	@endif
	@if($errors->any('email'))
	<script>
		toastr.error('El Email, No es correcto por favor verifique su email!')
	</script>
	@endif
	@if($errors->any('password_confirmation'))
	<script>
		toastr.error('La confimacion de la contraseña no es validad , profavor ingrese el mismo valor del campo anterios!')
	</script>
	@endif
</body>

</html>
