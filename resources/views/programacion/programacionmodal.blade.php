<!--Modal Actualizar-->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="box-title text-info" id="exampleModalCenterTitle"><i class="ti-user mr-15"></i>Actualizar información </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form class="form" id="editarprogramacion" action="{{ route('programacion.update','text') }}" method="POST">
              {{ method_field('patch') }}
              {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                      <input type="hidden" name="idprogramacion" id="programacion" value="">
                      <label>Nombre <span class="text-danger">*</span></label>
                      <input name="nombre" id="nombre" type="text" class="form-control" placeholder=" Nombre">
                      {!!$errors->first('nombre','<span class=error>:message</span>')!!}
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label>Inicio de horario <span class="text-danger">*</span></label>
                  <input name="inicio" id="inicio" type="date" class="form-control" placeholder="Inicio de horario">
                  {!!$errors->first('inicio','<span class=error>:message</span>')!!}
                </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label>Fin de horario</label>
                      <input name="fin" id="fin" type="date" class="form-control" placeholder="Fin de horario">
                      {!!$errors->first('fin','<span class=error>:message</span>')!!}
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label>Contenido <span class="text-danger">*</span></label>
                  <select class="form-control" name="idcontenido" id="contenido" value="{{ old('idcontenido') }}">
                    @foreach ($contenido as $cca)
                    <option value="{{ $cca->idcontenido }}">{{ $cca->titulo }}</option>
                    @endforeach
                  </select>
                  {!!$errors->first('idcontenido','<span class=error>:message</span>')!!}
                </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Docente <span class="text-danger">*</span></label>
                    <select class="form-control" name="idpersona" id="persona" value="{{ old('idpersona') }}">
                      @foreach ($persona as $cca)
                      <option value="{{ $cca->idpersona }}">{{ $cca->nombre }}</option>
                      @endforeach
                    </select>
                    {!!$errors->first('idpersona','<span class=error>:message</span>')!!}
                  </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label>Programación <span class="text-danger">*</span></label>
                  <select class="form-control" name="idgrupo" id="grupo" value="{{ old('idgrupo') }}">
                    @foreach ($grupo as $cca)
                    <option value="{{ $cca->idgrupo }}">{{ $cca->nombregrupo }}</option>
                    @endforeach
                  </select>
                  {!!$errors->first('idgrupo','<span class=error>:message</span>')!!}
                </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="modal-footer modal-footer-uniform">
                <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
                <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Actualizar</button>
            </div> 
          </form>
          <!-- /.box -->
      </div>
    </div>
  </div>
</div>


<!--Agregar-->
<div data-backdrop="static" data-keyboard="false" class="modal center-modal fade" id="modal-center" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Informacion de programación</h4>
              <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <div class="box">
                <!-- /.box-header -->
                
                <form class="form" id="programacion" action="{{ route('programacion.store') }}" method="POST">
                    {{ csrf_field() }}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                        <label>Nombre <span class="text-danger">*</span></label>
                        <input name="nombre" id="" type="text" class="form-control" placeholder="Programacion">
                        {!!$errors->first('nombre','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Inicio de horario <span class="text-danger">*</span></label>
                          <input name="inicio" id="" type="date" class="form-control" placeholder="Inicio de horario">
                          {!!$errors->first('inicio','<span class=error>:message</span>')!!}
                        </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Fin de horario <span class="text-danger">*</span></label>
                            <input name="fin" id="" type="date" class="form-control" placeholder="Inicio de horario">
                            {!!$errors->first('fin','<span class=error>:message</span>')!!}
                          </div>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                          <label>Contenido <span class="text-danger">*</span></label>
                          <select class="form-control" name="idcontenido" id="" value="{{ old('idcontenido') }}">
                            @foreach ($contenido as $cca)
                            <option value="{{ $cca->idcontenido }}">{{ $cca->titulo }}</option>
                            @endforeach
                          </select>
                          {!!$errors->first('idcontenido','<span class=error>:message</span>')!!}
                        </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Docente <span class="text-danger">*</span></label>
                            <select class="form-control" name="idpersona" id="" value="{{ old('idpersona') }}">
                              @foreach ($persona as $cca)
                              <option value="{{ $cca->idpersona }}">{{ $cca->nombre }}</option>
                              @endforeach
                            </select>
                            {!!$errors->first('idpersona','<span class=error>:message</span>')!!}
                          </div>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                          <label>Grupo <span class="text-danger">*</span></label>
                          <select class="form-control" name="idgrupo" id="" value="{{ old('idgrupo') }}">
                            @foreach ($grupo as $cca)
                            <option value="{{ $cca->idgrupo }}">{{ $cca->nombregrupo }}</option>
                            @endforeach
                          </select>
                          {!!$errors->first('idgrupo','<span class=error>:message</span>')!!}
                        </div>
                        </div>
                      </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="modal-footer modal-footer-uniform">
                      <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
                      <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Guardar</button>
                  </div> 
                </form>
                <!-- /.box -->			
          </div>
        </div>
      </div>