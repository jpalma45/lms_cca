@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">programación</li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="right-title">
        <button type="button" class="btn btn-success" data-toggle="modal" data-toggle="tooltip" title="Registrar Progranación" data-target="#modal-center">
			Registrar programación
    </button>

    </div>
  </div>
</div>

@endsection

@section('contenido')

<div class="col-12">
    <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">Programación</h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home8" role="tab"><span><i class="ion-home mr-15"></i>Activas</span></a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile8" role="tab"><span><i class="ion-person mr-15"></i>Inactivas</span></a> </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content tabcontent-border">
        <div class="tab-pane active" id="home8" role="tabpanel">
          <div class="p-15">

              <div class="box-body">
                  <div class="table-responsive">
                    <table id="example5" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>Nombre</th>
                       <th>Inicio</th>
                       <th>Fin</th>
                       <th>Operación </th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($programacion as $cca)
                     <tr>
                       <td>{{ $cca->nombre }}</td>
                       <td>{{ $cca->inicio }}</td>
                       <td>{{ $cca->fin }}</td>
                        <td>
                         <button type="button" class="btn btn-success mb-3"
                         data-myinicio={{ $cca->inicio }}
                         data-myfin={{ $cca->fin }}
                         data-mynombre={{ $cca->nombre }}
                         data-myidprogramacion={{ $cca->idprogramacion }}
                         data-myidcontenido={{ $cca->idcontenido }}
                         data-myidpersona={{ $cca->idpersona }}
                         data-myidgrupo={{ $cca->idgrupo }}
                         data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                         <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                        </button>
                         <form style="display: inline" method="POST" id="int" action="{{ route('programacion.destroy',$cca->idprogramacion)}}">
                             {!! csrf_field() !!}
                             {!! method_field('DELETE') !!}
                             <input type="hidden" name="idprogramacion" value="{{ $cca->idprogramacion }}">

                           <button type="button" class="btn btn-warning mb-3 sweetalert" data-toggle="tooltip" data-original-title="Inhabilitar"><a class="text-dark"><i class="ti-lock" aria-hidden="true"></i></a>
                        </form>
                       </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nombre</th>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>

          </div>
        </div>
        <div class="tab-pane" id="profile8" role="tabpanel">
          <div class="p-15">


              <div class="box-body">
                  <div class="table-responsive">
                    <table id="" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>Nombre</th>
                       <th>Inicio</th>
                       <th>Fin</th>
                       <th>Operación </th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($estado as $cca)
                     <tr>
                       <td>{{ $cca->nombre }}</td>
                       <td>{{ $cca->inicio }}</td>
                       <td>{{ $cca->fin }}</td>
                        <td>
                         <button type="button" class="btn btn-success mb-3"
                         data-myinicio={{ $cca->inicio }}
                         data-myfin={{ $cca->fin }}
                         data-mynombre={{ $cca->nombre }}
                         data-myidprogramacion={{ $cca->idprogramacion }}
                         data-myidcontenido={{ $cca->idcontenido }}
                         data-myidpersona={{ $cca->idpersona }}
                         data-myidgrupo={{ $cca->idgrupo }}
                         data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                         <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                        </button>
                         <form style="display: inline" method="POST" action="{{ route('programacion.activar',$cca->idprogramacion)}}">
                             {!! csrf_field() !!}
                             <input type="hidden" name="idprogramacion" value="{{ $cca->idprogramacion }}">

                           <button type="submit" class="btn btn-info mb-3" data-toggle="tooltip" data-original-title="Activar"><a class="text-dark"><i class="ti-unlock" aria-hidden="true"></i></a>
                        </form>
                       </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nombre</th>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>

          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

@include('programacion.programacionmodal')

@endsection

@section('script')

<script>
    $('#editar').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)
    var inicio = button.data('myinicio')
    var fin = button.data('myfin')
    var nombre = button.data('mynombre')
    var persona = button.data('myidpersona')
    var contenido = button.data('myidcontenido')
    var grupo = button.data('myidgrupo')
    var programacion_id = button.data('myidprogramacion')


    var modal = $(this)
    modal.find('.modal-body #nombre').val(nombre)
    modal.find('.modal-body #fin').val(fin)
    modal.find('.modal-body #inicio').val(inicio)
    modal.find('.modal-body #contenido').val(contenido)
    modal.find('.modal-body #persona').val(persona)
    modal.find('.modal-body #grupo').val(grupo)
    modal.find('.modal-body #programacion').val(programacion_id)


  });
  </script>

  @if($errors->any())
  <script>
  $('#modal-center').modal('show');
  </script>
  @endif

  @if($errors->any())
  <script>
  $('#').modal('show');
  </script>
  @endif
@endsection
