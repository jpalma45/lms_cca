<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">
      
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">
		  
        <li class="header nav-small-cap">Casa Cultural Andina</li>
		
		      <li>
            <a href="{{ Route('home.index') }}">
              <i class="ti-stats-down"></i>
              <span>Inicio</span>
            </a>
          </li>  
		  		
        <li class="header nav-small-cap">Gestion de Usuarios</li>
		  
        <li class="treeview">
          <a href="#">
            <i class="ti-email"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('users.index') }}"><i class="ti-more"></i>Usuarios</a></li>
          </ul>
        </li>


        <li class="header nav-small-cap">Gestion de Programación</li>
		  
        <li class="treeview">
          <a href="#">
            <i class="ti-pencil-alt"></i>
            <span>Programación</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('persona.index') }}"><i class="ti-more"></i>Persona</a></li>
            <li><a href="{{ route('aula.index') }}"><i class="ti-more"></i>Aula</a></li>	
            <li><a href="{{ route('matricula.index') }}"><i class="ti-more"></i>Matricula</a></li>	
            <li><a href="{{ route('programacion.index') }}"><i class="ti-more"></i>Programación</a></li>
          </ul>
        </li>  

        <li class="header nav-small-cap">Gestion de Contenido</li> 
		
        <li class="treeview">
          <a href="#">
            <i class="ti-write"></i>
		    	<span>Contenido</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('contenido.index') }}"><i class="ti-more"></i>Contendo</a></li>
          </ul>
        </li>

        <li class="header nav-small-cap">Gestion de Seguimiento</li> 
		  
        <li class="treeview">
          <a href="#">
            <i class="ti-pie-chart"></i>
			<span>Seguimiento</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('grupo.index') }}"><i class="ti-more"></i>Grupo</a></li>
            <li><a href="{{ route('vista.pdf') }}"><i class="ti-more"></i>Reportes</a></li>
          </ul>
        </li>
  
		    <li>
          <a href="/logout">
        <i class="ti-power-off"></i>
			      <span>Cerrar Sesion</span>
          </a>
        </li> 
        <li>
            <a href="{{ route('vista.acercade') }}">
          <i class="ti-info"></i>
              <span>Acerca de</span>
            </a>
          </li>
          <li>
            <a href="{{ route('vista.ayuda') }}">
          <i class="ti-info"></i>
              <span>Funcionamiento del sistema</span>
            </a>
          </li> 
        
      </ul>
    </section>
  </aside>