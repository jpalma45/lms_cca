<!--Agregar-->
<div data-backdrop="static" data-keyboard="false" class="modal center-modal fade" id="modal-center" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="box-title text-info"><i class="ti-user mr-15"></i> información de usuario</h4>
              <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <div class="box">
                <!-- /.box-header -->
                
                <form class="form" id="usuarioR" action="{{ route('users.store') }}" method="POST">
                    {{ csrf_field() }}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                        <label>Nombre de usuario <span class="text-danger">*</span></label>
                        <input id="form_username" name="name" id="" type="text" class="form-control" placeholder="Nombre">
                        <span id="username_error_message"></span>
                        {!!$errors->first('name','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Contraseña <span class="text-danger">*</span></label>
                          <input id="form_password" name="password" id="" type="password" class="form-control check-seguridad" placeholder="Contraseña">
                          <span id="password_error_message"></span>
                          {!!$errors->first('password','<span class=error>:message</span>')!!}
                        </div>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Confirmar contraseña <span class="text-danger">*</span></label>
                              <input name="confirt_password" id="form_retype_password" type="password" class="form-control" placeholder="Confimar Contraseña">
                              <span id="retype_password_error_message"></span>
                              {!!$errors->first('confirt_password','<span class=error>:message</span>')!!}
                            </div>
                            </div>
                          </div>
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                        <label >Correo electronico <span class="text-danger">*</span></label>
                        <input name="email" id="form_email" type="email" class="form-control" placeholder="Correo Electronico">
                        <span id="email_error_message"></span>
                        {!!$errors->first('email','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                    </div>

                      <div class="row">
                          <div class="col-md-12">
                          <div class="form-group">
                            <label>Tipo de persona <span class="text-danger">*</span></label>
                            <select class="form-control" name="rol" id="rol">
                                <option value="estudiante">Estudiante</option>
                                <option value="docente">Docente</option>
                                <option value="admin">Administrador</option>
                            </select>
                            {!!$errors->first('rol','<span class=error>:message</span>')!!}
                          </div>
                          </div>
                        </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="modal-footer modal-footer-uniform">
                      <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
                      <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Guardar</button>
                  </div> 
                </form>
                <!-- /.box -->			
          </div>
        </div>
      </div>