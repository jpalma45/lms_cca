@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="right-title">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-center">
						Registrar Usuario
        </button> 
        
    </div>
  </div>
</div>

@endsection

@section('contenido')

<div class="col-12">
    <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">Usuarios</h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home8" role="tab"><span><i class="ion-home mr-15"></i>Activos</span></a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile8" role="tab"><span><i class="ion-person mr-15"></i>Inactivos</span></a> </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content tabcontent-border">
        <div class="tab-pane active" id="home8" role="tabpanel">
          <div class="p-15">

              <div class="box-body">
                  <div class="table-responsive">
                    <table id="example5" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>Nombre</th>
                       <th>Email</th>
                       <th>Rol</th>
                       <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($usuario as $cca)
                     <tr>
                       <td>{{ $cca->name }}</td>
                       <td>{{ $cca->email }}</td>
                       <td>{{ $cca->rol }}</td>
                       <td>
                           <form style="display: inline" method="POST" id="int" action="{{ route('users.destroy',$cca->id)}}">
                               {!! csrf_field() !!}
                               {!! method_field('DELETE') !!}
                               <input type="hidden" name="iduser" value="{{ $cca->id }}">
             
                               <button type="button" id="sa-params" class="btn btn-danger mb-5 sweetalert"><i class="fa fa-check"></i> Inhabilitar</button>
                             </form>
                         </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>

          </div>
        </div>
        <div class="tab-pane" id="profile8" role="tabpanel">
          <div class="p-15">

              <div class="box-body">
                  <div class="table-responsive">
                    <table id="" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>Nombre</th>
                       <th>Email</th>
                       <th>Rol</th>
                       <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($estado as $cca)
                     <tr>
                       <td>{{ $cca->name }}</td>
                       <td>{{ $cca->email }}</td>
                       <td>{{ $cca->rol }}</td>
                       <td> 
                           <form style="display: inline" method="POST" action="{{ route('users.activar',$cca->id)}}">
                               {!! csrf_field() !!}
                               <input type="hidden" name="iduser" value="{{ $cca->id }}">
             
                               <button type="submit" class="btn btn-info mb-5"><i class="fa fa-check"></i> Activar</button>
                             </form>
                         </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>

          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

  @include('usuario.usuariomodal')
@endsection

@section('script')

<script>
    $('#editar').on('show.bs.modal', function (event) {
  
    var button = $(event.relatedTarget) 
    var name = button.data('myname')
    var email = button.data('myemail')
    var rol = button.data('myrol')
    var user_id = button.data('myiduser')
       
  
    var modal = $(this)
    modal.find('.modal-body #name').val(name)
    modal.find('.modal-body #email').val(email)
    modal.find('.modal-body #rol').val(rol)
    modal.find('.modal-body #user').val(user_id)

    
  });
  </script>

  @if($errors->any())
  <script>
  $('#modal-center').modal('show');
  </script>
  @endif

  @if($errors->any())
  <script>
  $('#').modal('show');
  </script>
  @endif
@ends
   
@endsection