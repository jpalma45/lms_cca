<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Programacion del grupo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @foreach ($programacion as $proo)
        
        <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label>Inicio de horario</label>
                    <input disabled value="{{ $proo->inicio }}" type="text" class="form-control" placeholder="">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label>Fin de horario</label>
                    <input disabled value="{{ $proo->fin }}" type="text" class="form-control" placeholder="">
                  </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                      <label>Nombre de programacion </label>
                      <input disabled value="{{ $proo->nombre }}" type="text" class="form-control" placeholder="">
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                      <label>Titulo de contenido</label>
                      <input disabled value="{{ $proo->titulo }}" type="text" class="form-control" placeholder="">
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label>Contenido del grupo</label>
                      <input disabled value="{{ $proo->contenido }}" type="text" class="form-control" placeholder="">
                    </div>
                    </div>
                  </div>
        </div>

        @endforeach
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>