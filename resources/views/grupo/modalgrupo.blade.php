
<!--Modal Actualizar-->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="box-title text-info" id="exampleModalCenterTitle"><i class="ti-user mr-15"></i>Actualizar información </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="editargrupo" action="{{ route('grupo.update','text') }}" method="POST">
          {{ method_field('patch') }}
          {{ csrf_field() }}
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
            <div class="form-group">
              <input type="hidden" name="idgrupo" id="grupo" value="">
              <label>Nombre grupo <span class="text-danger">*</span></label>
              <input name="nombregrupo" id="nombregrupo" type="text" class="form-control" placeholder="Nombre Grupo">
              {!!$errors->first('nombregrupo','<span class=error>:message</span>')!!}
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label>Cantidad de estudientes</label>
              <input name="cantidad" id="cantidad" type="number" class="form-control" placeholder="Cantidad De Estudientes">
              {!!$errors->first('cantidad','<span class=error>:message</span>')!!}
            </div>
            </div>
          </div>

            <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Aula <span class="text-danger">*</span></label>
                    <select class="form-control" name="aula_idaula" id="idaula">
                      @foreach ($aula as $cca)
                      <option value="{{ $cca->idaula }}">{{ $cca->nombre }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label> Horario</label>
                    <select class="form-control" name="horario" id="horario">
                    <option> Mañana</option>
                    <option> Tarde</option>
                    </select>
                  </div>
                </div>
              </div>
        </div>
        <!-- /.box-body -->
        <div class="modal-footer modal-footer-uniform">
            <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Actualizar</button>
        </div> 
      </form>
      </div>
    </div>
  </div>
</div>


<!--AGREGAR-->
  <div data-backdrop="static" data-keyboard="false" class="modal center-modal fade registrar" id="modal-center" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Informacion</h4>
              <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <div class="box">
                <!-- /.box-header -->
                
                <form class="form" id="modalgrupo" action="{{ route('grupo.store') }}" method="POST">
                    {{ csrf_field() }}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                        <label>Nombre de grupo <span class="text-danger">*</span></label>
                        <input name="nombregrupo" id="" type="text" class="form-control" placeholder="Nombre De Grupo">
                        {!!$errors->first('nombregrupo','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                      <div class="col-md-6">
                      <div class="form-group">
                        <label>Cantidad de integentes</label>
                        <input name="cantidad" id="" type="number" class="form-control" placeholder="Cantidad De Integentes">
                        {!!$errors->first('cantidad','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                    </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Aula <span class="text-danger">*</span></label>
                            <select class="form-control" name="aula_idaula">
                              @foreach ($aula as $cca)
                              <option value="{{ $cca->idaula }}">{{ $cca->nombre }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label> Horario</label>
                            <select class="form-control" name="horario" id="">
                            <option> Mañana</option>
                            <option> Tarde</option>
                            </select>
                          </div>
                        </div>
                      </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="modal-footer modal-footer-uniform">
                      <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
                      <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Guardar</button>
                  </div> 
                </form>
                <!-- /.box -->			
          </div>
        </div>
      </div>
