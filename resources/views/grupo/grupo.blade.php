@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Grupo</li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="right-title">
        <button type="button" class="btn btn-success" data-toggle="modal" data-toggle="tooltip" title="Registrar Aula" data-target="#modal-center">
						Registrar Grupo
        </button>
    </div>
  </div>
</div>

@endsection

@section('contenido')

<div class="col-12">
    <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">Grupos</h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home8" role="tab"><span><i class="ion-home mr-15"></i>Activos</span></a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile8" role="tab"><span><i class="ion-person mr-15"></i>Inactivos</span></a> </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content tabcontent-border">
        <div class="tab-pane active" id="home8" role="tabpanel">
          <div class="p-15">

              <div class="box-body">
                  <div class="table-responsive">
                    <table id="example5" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>Nombre Grupo</th>
                       <th>Cantidad de estudiantes</th>
                       <th>Nombre Auala</th>
                       <th>Operación </th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($grupo as $cca)
                     <tr>
                       <td>{{ $cca->nombregrupo }}</td>
                       <td>{{ $cca->cantidad }}</td>
                       <td>{{ $cca->nombre }}</td>
                        <td>
                         <button type="button" class="btn btn-success mb-3"
                         data-mynombre={{ $cca->nombregrupo }}
                         data-mycantidad={{ $cca->cantidad }}
                         data-myidaula={{ $cca->aula_idaula }}
                         data-myidgrupo={{ $cca->idgrupo }}
                         data-myhorario={{ $cca->horario }}
                         data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                         <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                        </button>

                         <form style="display: inline"  action="{{ route('grupo.show',$cca->idgrupo)}}">
                             <button type="submit" class="btn btn-info mb-3" data-toggle="tooltip" data-original-title="Detalles"><a class="text-dark"><i class="ti-eye" aria-hidden="true"></i></a>
                         </form>

                         <form style="display: inline" method="POST" id="int" action="{{ route('grupo.destroy',$cca->idgrupo)}}">
                             {!! csrf_field() !!}
                             {!! method_field('DELETE') !!}
                              <input type="hidden" name="idgrupo" value="{{ $cca->idgrupo }}">

                             <button type="button" class="btn btn-warning mb-3 sweetalert" data-toggle="tooltip" data-original-title="Inhabilitar"><a class="text-dark"><i class="ti-lock" aria-hidden="true"></i></a>
                          </form>
                       </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>

          </div>
        </div>
        <div class="tab-pane" id="profile8" role="tabpanel">
          <div class="p-15">

              <div class="box-body">
                  <div class="table-responsive">
                    <table id="" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>Nombre Grupo</th>
                       <th>Cantidad de estudiantes</th>
                       <th>Nombre Auala</th>
                       <th>Operación </th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($estadogrupo as $cca)
                     <tr>
                       <td>{{ $cca->nombregrupo }}</td>
                       <td>{{ $cca->cantidad }}</td>
                       <td>{{ $cca->nombre }}</td>
                        <td>
                         <button type="button" class="btn btn-success mb-3"
                         data-mynombre={{ $cca->nombregrupo }}
                         data-mycantidad={{ $cca->cantidad }}
                         data-myidaula={{ $cca->aula_idaula }}
                         data-myidgrupo={{ $cca->idgrupo }}
                         data-myhorario={{ $cca->horario }}
                         data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                         <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                        </button>

                         <form style="display: inline"  action="{{ route('grupo.show',$cca->idgrupo)}}">
                             <button type="submit" class="btn btn-info mb-3" data-toggle="tooltip" data-original-title="Detalles"><a class="text-dark"><i class="ti-eye" aria-hidden="true"></i></a>
                         </form>

                         <form style="display: inline" method="POST" action="{{ route('grupo.activar',$cca->idgrupo)}}">
                             {!! csrf_field() !!}
                              <input type="hidden" name="idgrupo" value="{{ $cca->idgrupo }}">

                             <button type="submit" class="btn btn-info mb-3" data-toggle="tooltip" data-original-title="Activar"><a class="text-dark"><i class="ti-unlock" aria-hidden="true"></i></a>
                          </form>
                       </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Cedula</th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>

          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

@include('grupo.modalgrupo')

@endsection

@section('script')

<script>
    $('#editar').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)
    var nombre = button.data('mynombre')
    var cantidad = button.data('mycantidad')
    var id_aula = button.data('myidaula')
    var grupo_id = button.data('myidgrupo')
    var horario = button.data('myhorario')
    var iddocente = button.data('myiddocente')



    var modal = $(this)
    modal.find('.modal-body #nombregrupo').val(nombre)
    modal.find('.modal-body #cantidad').val(cantidad)
    modal.find('.modal-body #idaula').val(id_aula)
    modal.find('.modal-body #horario').val(horario)
    modal.find('.modal-body #docentes').val(iddocente)
    modal.find('.modal-body #grupo').val(grupo_id)

  });
  </script>

  @if($errors->any())
  <script>
  $('#modal-center').modal('show');
  </script>
  @endif
@endsection
