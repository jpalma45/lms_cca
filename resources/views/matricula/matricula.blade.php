@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Matricula</li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="right-title">
        <button type="button" class="btn btn-success" data-toggle="modal" data-toggle="tooltip" title="Registrar Matricula" data-target="#modal-center">
						Registrar Matricula
        </button>
    </div>
  </div>
</div>

@endsection

@section('contenido')

<div class="col-12">
    <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">Matriculas</h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home8" role="tab"><span><i class="ion-home mr-15"></i>Activas</span></a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile8" role="tab"><span><i class="ion-person mr-15"></i>Inactivas</span></a> </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content tabcontent-border">
        <div class="tab-pane active" id="home8" role="tabpanel">
          <div class="p-15">

              <div class="box-body">
                  <div class="table-responsive">
                    <table id="example5" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>ID</th>
                       <th>Nombre</th>
                       <th>Cedula</th>
                       <th>Direcciòn</th>
                       <th>Telefono</th>
                       <th>Fechade nacimiento</th>
                       <th>Pago</th>
                       <th class="tamaño">Operación </th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($matriculas as $cca)
                     <tr>
                       <td>{{ $cca->idmatriculas }}</td>
                       <td>{{ $cca->nombre }}&nbsp;{{ $cca->apellido }}</td>
                       <td>{{{ $cca->cedula }}}</td>
                       <td>{{{ $cca->direccion }}}</td>
                       <td>{{{ $cca->telefono }}}</td>
                       <td>{{{ $cca->fecha_nacimiento }}}</td>
                       <td>{{ $cca->pago }}</td>
                       <td>
                       <button type="button" class="btn btn-success mb-3"
                           data-mypago={{ $cca->pago }}
                           data-myidgrupo={{ $cca->idgrupo }}
                           data-myidmatricula={{ $cca->idmatriculas }}
                           data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                           <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                         </button>

                      <form style="display: inline"  action="{{ route('matricula.show',$cca->idmatriculas)}}">
                          <button type="submit" class="btn btn-info mb-3" data-toggle="tooltip" data-original-title="Detalles"><a class="text-dark"><i class="ti-eye" aria-hidden="true"></i></a>
                      </form>

                       <form style="display: inline" method="POST" id="int" action="{{ route('matricula.destroy',$cca->idmatriculas)}}">
                         {!! csrf_field() !!}
                         {!! method_field('DELETE') !!}
                         <input type="hidden" name="idmatricula" value="{{ $cca->idmatriculas }}">

                         <button type="button" class="btn btn-warning mb-3 sweetalert" data-toggle="tooltip" data-original-title="Inhabilitar"><a class="text-dark"><i class="ti-lock" aria-hidden="true"></i></a>
                         </form>
                     </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>


          </div>
        </div>
        <div class="tab-pane" id="profile8" role="tabpanel">
          <div class="p-15">

              <div class="box-body">
                  <div class="table-responsive">
                    <table id="" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                       <th>ID</th>
                       <th>Nombre</th>
                       <th>Cedula</th>
                       <th>Direcciòn</th>
                       <th>Telefono</th>
                       <th>Fechade nacimiento</th>
                       <th>Pago</th>
                       <th class="tamaño">Operación </th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach ($estadoM as $cca)
                     <tr>
                       <td>{{ $cca->idmatriculas }}</td>
                       <td>{{ $cca->nombre }}&nbsp;{{ $cca->apellido }}</td>
                       <td>{{{ $cca->cedula }}}</td>
                       <td>{{{ $cca->direccion }}}</td>
                       <td>{{{ $cca->telefono }}}</td>
                       <td>{{{ $cca->fecha_nacimiento }}}</td>
                       <td>{{ $cca->pago }}</td>
                       <td>
                       <button type="button" class="btn btn-success mb-3"
                           data-mypago={{ $cca->pago }}
                           data-myidgrupo={{ $cca->idgrupo }}
                           data-myidmatricula={{ $cca->idmatriculas }}
                           data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                           <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                         </button>
                       <form style="display: inline" method="POST" action="{{ route('matricula.activar',$cca->idmatriculas)}}">
                         {!! csrf_field() !!}
                         <input type="hidden" name="idmatricula" value="{{ $cca->idmatriculas }}">

                         <button type="submit" class="btn btn-info mb-3" data-toggle="tooltip" data-original-title="Activar"><a class="text-dark"><i class="ti-unlock" aria-hidden="true"></i></a>
                         </form>
                     </td>
                     </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="display: none;"></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>

          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

  @include('matricula.modalmatricula')
@endsection

@section('script')

<script>
    $('#editar').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)
    var pago = button.data('mypago')
    var idgrupo = button.data('myidgrupo')
    var matricula_id = button.data('myidmatricula')


    var modal = $(this)
    modal.find('.modal-body #pago').val(pago)
    modal.find('.modal-body #idgrupo').val(idgrupo)
    modal.find('.modal-body #matricula').val(matricula_id)


  });
  </script>

  @if($errors->any())
  <script>
  $('#modal-center').modal('show');
  </script>
  @endif

  @if($errors->any())
  <script>
  $('#').modal('show');
  </script>
  @endif
@endsection
