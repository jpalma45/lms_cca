<!--Modal Actualizar-->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="box-title text-info" id="exampleModalCenterTitle"><i class="ti-user mr-15"></i>Actualizar Informacion</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="editarmatricula" action="{{ route('matricula.update','text') }}" method="POST"  autocomplete="off">
          {{ method_field('patch') }}
          {{ csrf_field() }}
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
              <div class="form-group">
                <input type="hidden" name="idmatriculas" id="matricula" value="">
                <label>Pago de matricula <span class="text-danger">*</span></label>
                <input disabled name="pago" id="pago" type="number" onkeyup="format(this)" onchange="format(this)" class="form-control" placeholder="Pago de Matricula">
                {!!$errors->first('pago','<span class=error>:message</span>')!!}
              </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                       <label>Grupo <span class="text-danger">*</span></label>
                         <select class="form-control" name="id_grupo" id="idgrupo">
                           @foreach ($grupos as $grupo)
                             <option id="idgrupo" value="{{ $grupo->idgrupo }}">{{ $grupo->nombregrupo }}</option>
                           @endforeach
                          </select>
                          {!!$errors->first('id_grupo','<span class=error>:message</span>')!!}  
                        </div>
                      </div>
                  </div>
          </div>
        <!-- /.box-body -->
        <div class="modal-footer modal-footer-uniform">
            <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Actualizar</button>
        </div> 
      </form>
      </div>
    </div>
  </div>
</div>


<!--AGREGAR-->
<div data-backdrop="static" data-keyboard="false" class="modal center-modal fade" id="modal-center" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Matricula</h4>
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <section class="content">
            <form class="form" id="modalmatricula" action="{{ route('matricula.store') }}" method="POST">
                {{ csrf_field() }}

                  <div class="box-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#inicio" role="tab"><span><i class="ion-home mr-15"></i>Matricula</span></a> </li>
                      <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#segundo" role="tab"><span><i class="ion-person mr-15"></i>Aprendices</span></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tabcontent-border">
                      <div class="tab-pane active" id="inicio" role="tabpanel">
                        <div class="p-15">

                            <div class="box-body">
                                <div class="row">
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    <label>Pago de matricula <span class="text-danger">*</span></label>
                                    <input name="pago" id=""  onkeyup="format(this)" onchange="format(this)" type="number" class="form-control" placeholder="Pago de Matricula">
                                    {!!$errors->first('pago','<span class=error>:message</span>')!!}
                                  </div>
                                  </div>
                                  <div class="col-md-12">
                                      <div class="form-group">
                                         <label>Grupo <span class="text-danger">*</span></label>
                                           <select class="form-control" name="idgrupo" id="">
                                             @foreach ($grupos as $grupo)
                                               <option id="idgrupo" value="{{ $grupo->idgrupo }}">{{ $grupo->nombregrupo }}</option>
                                             @endforeach
                                         </select>
                                         {!! $errors->first('idgrupo','<spanclass=error>:message</span>') !!}
                                      </div>
                                   </div>
                                </div>
                              </div>
                          
                        </div>
                      </div>
                      <div class="tab-pane" id="segundo" role="tabpanel">
                        <div class="p-15">

                            <div class="row">
            
                                <!--Shadowed box!-->
                                <div class="col-15">
                    
                    
                                   <!-- /.box-header -->
                                   <div class="box-body">
                                     <div class="table-responsive">
                                       <table id="example1" class="table table-bordered table-striped" data-page-length='2'>
                                       <thead>
                                         <tr>
                                           <th>Seleccion</th>
                                           <th>nombre</th>
                                           <th>Documento</th>
                                         </tr>
                                       </thead>
                                       <tbody>
                                        @foreach ($personas as $persona)
                                       <tr>
                                        <td>
                                        <div class="checkbox">
                                          <input type="checkbox" id="{{ $persona->idpersona }}" name="idpersona[]" value="{{ $persona->idpersona }}">
                                          <label for="{{ $persona->idpersona }}"></label>
                                        </div>
                                        </td>
                                        <td>{{ $persona->nombre }}</td>
                                        <td>{{ $persona->cedula }}</td>
                                       </tr>
                                        @endforeach
                                       </tbody>
                                       <tfoot>
                                         <tr>
                                           <th>Seleccion</th>
                                           <th>Nombre</th>
                                           <th>Documento</th>
                                           {!! $errors->first('idpersona','<spanclass=error>:message</span>') !!}
                                         </tr>
                                       </tfoot>
                                       </table>
                                     </div>
                                   </div>
                                   <!-- /.box-body -->
                                   <!-- /.box -->
                           
                                   <!-- /.box -->          
                                 </div>
                        
                              </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>


          <!-- /.box-body -->
          <div class="modal-footer modal-footer-uniform">
              <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
              <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Guardar</button>
          </div> 
        <!-- /.box -->
        </form>
        </section>

    </div>
  </div>