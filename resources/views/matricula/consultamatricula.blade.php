@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">matriculas</li>
                <li class="breadcrumb-item active" aria-current="page">Detalles de matricula</li>
            </ol>
        </nav>
      </div>
    </div>
  </div>
</div>

@endsection

@section('contenido')

<section  style = "width: 100%">

    <div class="row">
        <div class="col-12">
          <h4 class="page-header">Informacion de matricula</h4>
        </div>	

        <div class="col-12">
          <div class="box box-inverse bg-img" style="background-image: url(../../images/gallery/full/1.jpg);" data-overlay="2">

            <div class="box-body text-center pb-50">
              <a href="#">
                <img class="avatar avatar-xxl avatar-bordered" src="../../images/avatar/logo.png" alt="">
              </a>
              @foreach ($detalles as $cca)
              <h4 class="mt-2 mb-0"><a class="hover-primary text-white" href="#">{{ $cca->nombre }}</a></h4>
              <span><i class="fa fa-map-marker w-20"></i> {{ $cca->direccion }}</span>
            </div>

            <ul class="box-body flexbox flex-justified text-center" data-overlay="4">
              <li>
                <span class="opacity-60">Nombre completo</span><br>
                <span class="font-size-20">{{ $cca->nombre }}&nbsp;{{ $cca->snombre }}&nbsp;{{ $cca->apellido }}</span>
              </li>
              <li>
                <span class="opacity-60">Cedula</span><br>
                <span class="font-size-20">{{ $cca->cedula }}</span>
              </li>
              <li>
                <span class="opacity-60">Direccion</span><br>
                <span class="font-size-20">{{ $cca->direccion }}</span>
              </li>
              <li>
                <span class="opacity-60">Telefono</span><br>
                <span class="font-size-20">{{ $cca->telefono }}</span>
              </li>
              <li>
                <span class="opacity-60">Telefono</span><br>
                <span class="font-size-20">{{ $cca->telefono }}</span>
              </li>
            </ul>

            
            <ul class="box-body flexbox flex-justified text-center" data-overlay="4">
                <li>
                  <span class="opacity-60">Fecha de nacimiento</span><br>
                  <span class="font-size-20">{{ $cca->fecha_nacimiento }}</span>
                </li>
                <li>
                  <span class="opacity-60">Correo</span><br>
                  <span class="font-size-20">{{ $cca->email }}</span>
                </li>
                <li>
                  <span class="opacity-60">Grupo</span><br>
                  <span class="font-size-20">{{ $cca->nombregrupo }}</span>
                </li>
                <li>
                  <span class="opacity-60">Horario</span><br>
                  <span class="font-size-20">{{ $cca->horario }}</span>
                </li>
                <li>
                    <span class="opacity-60">Pago</span><br>
                    <span class="font-size-20">{{ $cca->pago }}</span>
                  </li>
              </ul>

              @endforeach
          </div>
        </div>
    </div>
  </section>

@endsection
