@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
            <li class="breadcrumb-item " aria-current="page">Usuarios</li>
            <li class="breadcrumb-item active" aria-current="page">Perfil</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>


@endsection


@section('contenido')


        <div class="row">
          <div class="col-12 col-lg-7 col-xl-8">
              
            <div class="nav-tabs-custom box-profile">

                  <div class="box p-15">	
                    
            @foreach ($user as $usuario)
                
            @endforeach  

            <form class="form" enctype="multipart/form-data" id="perfil" action="{{ route('users.update',$id) }}" method="POST">
              {{ method_field('patch') }}
              {{ csrf_field() }}
              <div class="box-body">
							<div class="form-group">
								<label>Nombre</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="ti-user"></i></span>
									</div>
									<input name="name" value="{{ auth()->user()->name }}" id="inputName" type="text" class="form-control" placeholder="Nombre de Usuario">
                  {!!$errors->first('name','<span class=error>:message</span>')!!}
                </div>
							</div>
							<div class="form-group">
								<label>Correo</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="ti-email"></i></span>
									</div>
									<input name="email" value="{{ auth()->user()->email }}" id="inputEmail" type="email" class="form-control" placeholder="Correo Electronico">
                  {!!$errors->first('email','<span class=error>:message</span>')!!}
                </div>
              </div>
              <div  id="checboxpass" style="display: none;">
                <div id="xpass">
              <div class="form-group" >
								<label>Contraseña</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="ti-lock"></i></span>
									</div>
									<input style="width:525px;height:35px" name="password" value="" id="inputpassword" type="password" class="form-control check-seguridad" placeholder="Contraseña de usuario">
                  {!!$errors->first('password','<span class=error>:message</span>')!!}
                </div>
              </div>
              <div class="form-group">
                  <label>Confirmar Contraseña</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ti-lock"></i></span>
                    </div>
                    <input name="confirt_password" value="" id="confir_password" type="password" class="form-control" placeholder="Confirmar Contraseña de usuario">
                  </div>
                </div>
              </div>
              </div>
							<div class="form-group">
								<label>Foto de perfil</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="ti-upload"></i></span>
									</div>
                  <input width="70%" name="avatar" id="inputAvatar" type="file" class="form-control" placeholder="Avatar">
                  {!!$errors->first('avatar','<span class=error>:message</span>')!!}
								</div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <fieldset class="controls">
                      <input name="password" type="radio" id="radio_pass" value="1" onchange="javascript:showContent()">
                      <label for="radio_pass">Editar contraseña</label>
                    </fieldset>
                  </div>
                </div>
              </div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-outline">
							  <i class="ti-save-alt"></i> Actualizar
							</button>
						</div>  
					</form>
                  </div>			  
                <!-- /.tab-pane -->
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->		

            <div class="col-12 col-lg-5 col-xl-4">
                
                <div class="box box-inverse bg-img" style="background-image: url(../../images/gallery/full/1.jpg);" data-overlay="2">

                    <div class="box-body text-center pb-50">
                      <a href="#">
                        <img class="avatar avatar-xxl avatar-bordered" src="{{ Storage::url($usuario->avatar) }}" alt="">
                      </a>
                      <h4 class="mt-2 mb-0"><a class="hover-primary text-white" href="#">{{ auth()->user()->name }}</a></h4>
                    </div>
                  </div>				

                <!-- Profile Image -->
                <div class="box">
                  <div class="box-body box-profile">            
                    <div class="row">
                      <div class="col-12">
                          <div>
                              <p>Coreo :<span class="text-gray pl-10">{{ auth()->user()->email }}</span> </p>
                              <p>Rol :<span class="text-gray pl-10">{{ auth()->user()->rol }}</span></p>
                              <p>Entidad :<span class="text-gray pl-10">Casa Cultural Andina</span></p>
                          </div>
                      </div>
                      <div class="col-12">
                          <div class="pb-15">						
                             
                          </div>
                      </div>
                      <div class="col-12">
                          <div>
                              <div class="map-box">
                                  <iframe width="100%" height="100" frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>

            </div>

        </div>
        <!-- /.row -->

@endsection

@section('script')
@if($errors->any())
toastr.error('No, se apodido actualizar tus datos!')
@endif
@endsection