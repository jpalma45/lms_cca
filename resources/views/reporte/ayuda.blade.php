@extends('layout.index')

@section('top')

<div class="content-header">
<div class="d-flex align-items-center">
<div class="mr-auto">
<h3 class="page-title">Inicio</h3>
<div class="d-inline-block align-items-center">
<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
        <li class="breadcrumb-item" aria-current="page">Ayudas</li>
    </ol>
</nav>
</div>
</div>
</div>
</div>

@endsection

@section('contenido')

	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../plantilla/bootstrap/dist/css/bootstrap.min.css">

	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../css/bootstrap-extend.css">

	<!--alerts CSS -->
    <link href="../plantilla/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

	<!-- Theme style -->
	<link rel="stylesheet" href="../css/master_style.css">

	<!-- UltimatePro Admin skins -->
    <link rel="stylesheet" href="../css/skins/_all-skins.css">

<section class="content">
<div class="row">

<div class="col-md-6 col-12">
<div class="box">
    <div class="box-header with-border">
    <h4 class="box-title">Usuario</strong></h4>
    </div>
    <div class="box-body">
    <p>Similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
    </div>
    <div class="box-footer text-right">
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal2">
        Documentacion
    </button>
    </div>
</div>
</div>

<div class="col-md-6 col-12">
    <div class="box">
        <div class="box-header with-border">
        <h4 class="box-title">Persona</strong></h4>
        </div>
        <div class="box-body">
        <p>Similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
        </div>
        <div class="box-footer text-right">
        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg">
            Documentacion
        </button>
        </div>
    </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6 col-12">
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Aula</strong></h4>
            </div>
            <div class="box-body">
                <p>Similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
            </div>
            <div class="box-footer text-right">
                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal3">
                    Documentacion
                </button>
            </div>
            </div>
        </div>

        <div class="col-md-6 col-12">
                <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Matricula</strong></h4>
                </div>
                <div class="box-body">
                    <p>Similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                </div>
                <div class="box-footer text-right">
                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal4">
                       Documentacion
                    </button>
                </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 col-12">
                    <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Programacion</strong></h4>
                    </div>
                    <div class="box-body">
                        <p>Similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                    </div>
                    <div class="box-footer text-right">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal5">
                            Documentacion
                        </button>
                    </div>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                        <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Contenido</strong></h4>
                        </div>
                        <div class="box-body">
                            <p>Similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                        </div>
                        <div class="box-footer text-right">
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal6">
                                Documentacion
                            </button>
                        </div>
                        </div>
                    </div>
                </div>
        <div class="row">

            <div class="col-md-6 col-12">
                    <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Grupo</strong></h4>
                    </div>
                    <div class="box-body">
                        <p>Similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                    </div>
                    <div class="box-footer text-right">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal7">
                            Documentacion
                        </button>
                    </div>
                    </div>
                </div>

                </div>

          <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Persona</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>

                        		 <!-- Step wizard -->
		  <div class="box box-default">
                <!-- /.box-header -->
                <div class="box-body wizard-content">
                    <form action="close" class="tab-wizard wizard-circle">
                        <!-- Step 1 -->
                        <h6>Campos Obligatorios</h6>
                        <section>
                            <p>
                                Para crear una nueva persona se debe ingresar en el formulario de registro, este campo es
                                 obligatorio ya que está marcado con un asterisco (*)
                                 y el sistema no permitirá crear una persona sin los campos especificados con obligatorios. <br />
                                <img src="../images/ayudas/errorpersona.png" alt="Error de registro" width="700px" height="350px"/><br>
                                Luego de ingresar todos los campos marcados como obligatorios, se deberá marcar un rol para la persona
                            </p>
                        </section>
                        <!-- Step 2 -->
                        <h6>Resgistro de persona</h6>
                        <section>
                            <p>
                                Para guardar la persona, se debe pulsar con el botón izquierdo del mouse sobre el botón
                                “Guardar”, ubicado en la parte inferior izquierda de la ventana del formulario. <br />
                                <img src="../images/ayudas/registropersona.png" alt="Registro de persona" width="700px" height="350px"/><br>
                                Luego de pulsar en el botón “Guardar”, el sistema muestra una alerta confirmando que el rol ha sido registrado.
                            </p>
                        </section>
                        <!-- Step 3 -->
                        <h6>Actualizar persona</h6>
                        <section>
                            <p>
                                Al terminar de editar los datos de la persona que requieran cambio, se debe pulsar con el botón
                                izquierdo del mouse sobre el botón “Guardar”, ubicado en la parte inferior izquierda del formulario.  <br />
                                <img src="../images/ayudas/actualizarpersona.png" alt="Registro de persona" width="700px" height="350px"/><br>
                                Si no se desea editar ningún dato de la persona, se debe pulsar con el botón izquierdo del mouse
                                en el botón “Cancelar”, ubicado en la parte inferior derecha del formulario.
                                <br>
                                Para cerrar la ventana sin hacer cambios en los datos de la persona, se debe pulsar con el botón
                                izquierdo del mouse sobre la equis (x) ubicada en la parte superior derecha de la ventana.

                            </p>
                        </section>
                        <!-- Step 4 -->
                        <h6>Listar persona</h6>
                        <section>
                            Al entrar en la sección de roles del módulo de Gestión, el sistema muestra la lista de personas registradas en el sistema. <br>
                            <img src="../images/ayudas/listarpersona.png" alt="Registro de persona" width="700px" height="350px"/><br>
                            Para buscar una persona dentro de la lista de personas, se debe ingresar los datos del mismo en la barra de búsqueda ubicada en la parte superior de la tabla de personas.

                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
      <!-- /.modal -->

      <div class="modal fade bs-example-modal-lg" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Usuario</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>

		  <!-- vertical wizard -->
		  <div class="box box-default">
                <!-- /.box-header -->
                <div class="box-body wizard-content">
                    <form action="#" class="tab-wizard vertical wizard-circle">
                        <!-- Step 1 -->
                        <h6>Registrar Usuario</h6>
                        <section>
                        <p>
                            Para registrar un nuevo usuario, se deben ingresar los datos de éste en el formulario de crear usuario.
                            Los campos con el asterisco (*) son obligatorios. <br>

                            Si alguno de los campos obligatorios no es diligenciado correctamente, el sistema mostrará un mensaje de
                            error sobre el campo y no permitirá el registro del usuario hasta que los campos obligatorios sean
                            ingresados correctamente. <br>

                            <img src="../images/ayudas/registrousuario.png" alt="Error de registro" width="700px" height="350px"/><br>
                            Cuando todos los campos estén diligenciados correctamente se activará el botón “Guardar”.
                         </p>
                        </section>
                        <!-- Step 2 -->
                        <h6>Actualizar perfil</h6>
                        <section>
                            <p>
                             Después de ingresar a la sección de perfil de usuario como se indica en el numeral “2.1.1 Perfil”, se pulsa sobre el título “perfil” con el
                             botón izquierdo del mouse y se despliega el formulario para editar los datos básicos del perfil de usuario. <br>
                             <img src="../images/ayudas/actualizarperfil.png" alt="Error de registro" width="700px" height="350px"/><br>

                            </p>
                        </section>
                        <!-- Step 3 -->
                        <h6>Listar usuario</h6>
                        <section>
                            <p>
                                    Al entrar en la sección de usuarios del módulo de Gestión, el sistema muestra la lista de usuarios registrados en el sistema. <br>
                               <img src="../images/ayudas/listarusuario.png" alt="Error de registro" width="700px" height="350px"/><br>
                               Para buscar un usuario dentro de la lista de usuarios, se debe ingresar los datos del mismo en la barra de búsqueda ubicada en la parte superior de la tabla de usuarios.

                            </p>
                        </section>
                        <!-- Step 4 -->
                        <h6>Cambiar estado de usuario</h6>
                        <section>
                           <p>
                             Al pulsar con el botón izquierdo del mouse sobre el botón con el texto inhabilitar, se muestra una alerta de confirmación
                             para inhabilitar el usuario respectivo al botón. En caso de ser confirmada la acción en el botón “Inhabilitar”, el sistema inhabilita
                              al usuario. Si se pulsa sobre el botón “Cancelar” el sistema cierra la alerta sin inhabilitar al usuario. <br>
                            <img src="../images/ayudas/estadousuario.png" alt="Error de registro" width="200px" height="100px"/><br>
                             <h5>Activar</h5>
                             Después de cambiar ala pestaña de usuarios inactivos para mostrar usuarios inhabilitados, y en el caso de que existan efectivamente usuarios inhabilitados; al pulsar con el
                              botón izquierdo del mouse sobre el botón activar para darle acceso a la aplicación a el usuario <br>
                              <img src="../images/ayudas/estadousuario2.png" alt="Error de registro" width="200px" height="100px"/><br>

                           </p>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
      <!-- /.modal -->
      <div class="modal fade bs-example-modal-lg" id="modal3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Aula</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>

		  <!-- vertical wizard -->
		  <div class="box box-default">
                <!-- /.box-header -->
                <div class="box-body wizard-content">
                    <form action="#" class="tab-wizard vertical wizard-circle">
                        <!-- Step 1 -->
                        <h6>Registro de aula</h6>
                        <section>
                            <p>
                            Para crear una nueva aula, se debe ingresar el nombre en el formulario de registro, este campo es obligatorio
                             ya que está marcado con un asterisco (*) y el sistema no permitirá crear un aula sin nombre. <br>
                            <img src="../images/ayudas/registroaula.png" alt="Error de registro" width="700px" height="350px"/><br>
                            Luego de pulsar en el botón “Guardar”, el sistema muestra una alerta confirmando que el aula ha sido registrada.

                            </p>
                        </section>
                        <!-- Step 2 -->
                        <h6>Editar aula</h6>
                        <section>
                            <p>
                            se pueden editar todos los datos de éste. El campo “Nombre” tiene el asterisco (*), por lo tanto, es obligatorio y no debe estar vacío.
                            Al terminar de editar los datos del aula que requieran cambio, se debe pulsar con el botón izquierdo del mouse sobre el botón “Guardar”,
                            ubicado en la parte inferior derecha del formulario. <br>

                            <img src="../images/ayudas/editaraula.png" alt="Error de registro" width="700px" height="350px"/><br>
                            Si no se desea editar ningún dato del aula, se debe pulsar con el botón izquierdo del mouse en el botón “Cancelar”, ubicado en la parte
                             inferior derecha del formulario. <br>

                            Para cerrar la ventana sin hacer cambios en los datos del aula, se debe pulsar con el botón izquierdo del mouse sobre la equis (x),
                            ubicada en la parte superior derecha de la ventana.
                            </p>
                        </section>
                        <!-- Step 3 -->
                        <h6>Listar aula</h6>
                        <section>
                         <p>
                           Al entrar en la sección de aulas del módulo de programación, el sistema muestra la lista de aulas registradas en el sistema. <br>
                          <img src="../images/ayudas/listaraula.png" alt="Error de registro" width="700px" height="350px"/><br>
                          Para buscar un aula dentro de la lista de aulas, se debe ingresar los datos de la misma en la barra de búsqueda, ubicada en la parte superior de la tabla de aulas.
                         </p>
                        </section>
                        <!-- Step 4 -->
                        <h6>Cambiar estado de aula</h6>
                        <section>
                          <p>
                              <h5>inhabilitar aula</h5>
                            Al pulsar con el botón izquierdo del mouse, sobre el botón candado, se muestra una alerta de confirmación,
                            para inhabilitar el aula respectiva al botón. En caso de ser confirmada la acción en el botón “Inhabilitar”,
                            el sistema inhabilita el aula. Si se pulsa sobre el botón “Cancelar”, el sistema cierra la alerta sin inhabilitar el aula. <br>
                            <img src="../images/ayudas/estadoaula.png" alt="Error de registro" width="200px" height="100px"/><br>
                            <h5>Activar aula</h5>
                            Después de cambiar la pestaña para mostrar las aulas inhabilitadas, y en el caso de que existan efectivamente aulas inhabilitadas;
                            al pulsar con el botón izquierdo del mouse sobre el botón candado abierto, muestra una alerta de confirmación del cambio de estado <br>
                            <img src="../images/ayudas/estadoaula2.png" alt="Error de registro" width="200px" height="100px"/><br>

                          </p>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg" id="modal4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Matricula</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>

              <!-- vertical wizard -->
              <div class="box box-default">
                    <!-- /.box-header -->
                    <div class="box-body wizard-content">
                        <form action="#" class="tab-wizard vertical wizard-circle">
                            <!-- Step 1 -->
                            <h6>Registrar Matricula</h6>
                            <section>
                              <p>
                                    Para registrar un nueva matricula, se deben ingresar los datos de éste en el formulario de crear matricula.
                                    Los campos con el asterisco (*), son obligatorios. <br>

                                     Si alguno de los campos obligatorios no es diligenciado correctamente, el sistema mostrará un mensaje de
                                     error sobre el campo y no permitirá el registro de la matricula hasta que los campos obligatorios sean
                                     ingresados correctamente. <br>

                                   <img src="../images/ayudas/registromatricula.png" alt="Error de registro" width="700px" height="350px"/><br>
                                   Al pulsar sobre el botón “Guardar”, con el botón izquierdo del mouse, el sistema muestra una alerta confirmando
                                   el registro del nuevo profesional en la base de datos.
                              </p>
                            </section>
                            <!-- Step 2 -->
                            <h6>Editar Matricula</h6>
                            <section>
                                <p>
                                Al pulsar con el botón izquierdo del mouse sobre el botón lápiz, se muestra una ventana con la información de la matrícula.
                                . Los campos que tienen el asterisco (*) son campos obligatorios y por lo tanto no deben estar vacíos.  <br>

                                Al terminar de editar los datos del profesional que requieran cambio, se debe pulsar con el botón izquierdo del mouse sobre
                                el botón “Guardar”, ubicado en la parte inferior derecha del formulario. <br>

                                  <img src="../images/ayudas/editarmatricula.png" alt="Error de registro" width="700px" height="350px"/><br>
                                  Si no se desea editar ningún dato de la matrícula, se debe pulsar con el botón izquierdo del mouse en el botón “Cancelar”,
                                   ubicado en la parte inferior derecha del formulario. <br>

                                  Para cerrar la ventana sin hacer cambios en los datos del profesional se debe pulsar botón con el botón izquierdo del mouse
                                   sobre la equis (x), ubicada en la parte superior derecha de la ventana. <br>

                                </p>
                            </section>
                            <!-- Step 3 -->
                            <h6>Listar matricula</h6>
                            <section>
                                <p>
                                    Al entrar en la sección de matrícula del módulo de matricula, el sistema muestra la lista de matrículas registradas en el sistema. <br>
                                   <img src="../images/ayudas/listarmatricula.png" alt="Error de registro" width="700px" height="350px"/><br>
                                   Para buscar una matrícula dentro de la lista de matriculas, se debe ingresar los datos de la misma en la barra de búsqueda ubicada en
                                   la parte superior de la tabla de matrículas.

                                </p>
                            </section>
                            <!-- Step 4 -->
                            <h6>Cambiar estado de matricula</h6>
                            <section>
                                <p>
                                    <h5>inhabilitar matricula</h5>
                                    Al pulsar con el botón izquierdo del mouse sobre el botón candado, se muestra una alerta de confirmación para
                                    inhabilitar la matricula respectiva al botón. En caso de ser confirmada la acción en el botón “Inhabilitar”,
                                    el sistema inhabilita la matrícula. Si se pulsa sobre el botón “Cancelar”, el sistema cierra la alerta sin inhabilitar la matrícula. <br>
                                 <img src="../images/ayudas/estadomatricula.png" alt="Error de registro" width="200px" height="100px"/><br>
                                 <h5>Activar</h5>
                                 Después de cambiar la pestaña para mostrar las matrículas inhabilitadas, y en el caso de que existan efectivamente matriculas inhabilitadas;
                                 al pulsar con el botón izquierdo del mouse sobre el botón candado abierto, muestra una alerta de confirmación del cambio de estado <br>
                                 <img src="../images/ayudas/estadomatricula2.png" alt="Error de registro" width="200px" height="100px"/><br>


                                </p>
                            </section>
                        </form>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <div class="modal fade bs-example-modal-lg" id="modal5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myLargeModalLabel">Programacion</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>

                  <!-- vertical wizard -->
                  <div class="box box-default">
                        <!-- /.box-header -->
                        <div class="box-body wizard-content">
                            <form action="#" class="tab-wizard vertical wizard-circle">
                                <!-- Step 1 -->
                                <h6>Registrar Programación</h6>
                                <section>
                                    <p>
                                    Para crear una nueva programación se debe ingresar en el formulario de registro, este campo es obligatorio
                                    ya que está marcado con un asterisco (*) y el sistema no permitirá crear una programación sin nombre, fecha de inicio y fecha final. <br>
                                    <img src="../images/ayudas/registroprogramacion.png" alt="Error de registro" width="700px" height="350px"/><br>
                                    Para guardar la programación, se debe pulsar con el botón izquierdo del mouse sobre el botón “Guardar”, ubicado en la parte inferior derecha de la ventana del formulario.
                                    </p>
                                </section>
                                <!-- Step 2 -->
                                <h6>Edtar programación</h6>
                                <section>
                                    <p>
                                    Al terminar de editar los datos de la programación que requieran cambio, se debe pulsar con el botón
                                    izquierdo del mouse sobre el botón “Actualizar”, ubicado en la parte inferior izquierda del formulario. <br>
                                    <img src="../images/ayudas/editarprogramacion.png" alt="Error de registro" width="700px" height="350px"/><br>
                                    Si no se desea editar ningún dato de la programación, se debe pulsar con el botón izquierdo del mouse en el botón
                                    “Cancelar”, ubicado en la parte inferior derecha del formulario. <br>

                                    Para cerrar la ventana sin hacer cambios en los datos de la programación, se debe pulsar con el botón izquierdo del mouse sobre la equis (x)
                                    ubicada en la parte superior derecha de la ventana. <br>

                                    </p>
                                </section>
                                <!-- Step 3 -->
                                <h6>Listar programación</h6>
                                <section>
                                   <p>
                                    Al entrar en la sección de programaciones del módulo de Gestión de programación,
                                    el sistema muestra la lista de programaciones registradas en el sistema. <br>
                                    <img src="../images/ayudas/listarprogramacion.png" alt="Error de registro" width="700px" height="350px"/><br>
                                    Para buscar una programación dentro de la lista de programaciones, se debe ingresar los datos del mismo en la
                                    barra de búsqueda ubicada en la parte superior de la tabla de programaciones.
                                   </p>
                                </section>
                                <!-- Step 4 -->
                                <h6>Cambiar estado de programación</h6>
                                <section>
                                    <p>
                                        <h5>inhabilitar programación</h5>
                                        Al pulsar con el botón izquierdo del mouse sobre el botón candado, se muestra una alerta de confirmación para inhabilitar la programación respectiva al botón. En caso de ser confirmada la acción en el botón “Inhabilitar”, el sistema inhabilita la programación.
                                        Si se pulsa sobre el botón “Cancelar”, el sistema cierra la alerta sin inhabilitar la programación. <br>
                                     <img src="../images/ayudas/estadoprogramacion.png" alt="Error de registro" width="200px" height="100px"/><br>
                                        <h5>Activar programación</h5>
                                        Después de cambiar la pestaña para mostrar las programaciones inhabilitadas, y en el caso de que existan efectivamente programaciones inhabilitadas; al pulsar con el botón izquierdo
                                        del mouse sobre el botón candado abierto, muestra una alerta de confirmación del cambio de estado <br>
                                        <img src="../images/ayudas/estadoprogramacion2.png" alt="Error de registro" width="200px" height="100px"/><br>

                                    </p>
                                </section>
                            </form>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <div class="modal fade bs-example-modal-lg" id="modal6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myLargeModalLabel">Contenido</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>

                      <!-- vertical wizard -->
                      <div class="box box-default">
                            <!-- /.box-header -->
                            <div class="box-body wizard-content">
                                <form action="#" class="tab-wizard vertical wizard-circle">
                                    <!-- Step 1 -->
                                    <h6>Registrar contenido</h6>
                                    <section>
                                        <p>
                                            Para crear un nuevo contenido se debe ingresar en el formulario de registro, este campo es obligatorio
                                            ya que está marcado con un asterisco (*) y el sistema no permitirá crear un contenido sin nombre y
                                            descripción. <br>
                                         <img src="../images/ayudas/registrocontenido.png" alt="Error de registro" width="700px" height="350px"/><br>
                                         Para guardar el contenido, se debe pulsar con el botón izquierdo del mouse sobre el botón “Guardar”,
                                         ubicado en la parte inferior derecha de la ventana del formulario.
                                        </p>
                                    </section>
                                    <!-- Step 2 -->
                                    <h6>Editar contenido</h6>
                                    <section>
                                        <p>
                                        Al terminar de editar los datos del contenido que requieran cambio, se debe pulsar con el botón izquierdo
                                        del mouse sobre el botón “Actualizar”, ubicado en la parte inferior izquierda del formulario. <br>
                                         <img src="../images/ayudas/editarcontenido.png" alt="Error de registro" width="700px" height="350px"/><br>
                                         Si no se desea editar ningún dato del contenido, se debe pulsar con el botón izquierdo del mouse en el botón “Cancelar”,
                                         ubicado en la parte inferior derecha del formulario. <br>

                                         Para cerrar la ventana sin hacer cambios en los datos del contenido, se debe pulsar con el botón izquierdo del mouse sobre
                                         la equis (x) ubicada en la parte superior derecha de la ventana. <br>
                                        </p>
                                    </section>
                                    <!-- Step 3 -->
                                    <h6>Listar contenido</h6>
                                    <section>
                                        <p>
                                        Al entrar en la sección de contenido del módulo de Gestión de contenido,
                                        el sistema muestra la lista de los contenidos registrados en el sistema. <br>
                                        <img src="../images/ayudas/listarcontenido.png" alt="Error de registro" width="700px" height="350px"/><br>
                                        Para buscar un contenido dentro de la lista de contenido, se debe ingresar los
                                        datos del mismo en la barra de búsqueda ubicada en la parte superior de la tabla de contenidos.
                                        </p>
                                    </section>
                                    <!-- Step 4 -->
                                    <h6>Cambiar estado</h6>
                                    <section>
                                     <p>
                                         <h5>inhabilitar contenido</h5>
                                         Al pulsar con el botón izquierdo del mouse sobre el botón candado, se muestra una alerta de confirmación
                                          para inhabilitar el contenido respectivo a el botón. En caso de ser confirmada la acción en el botón
                                          “Inhabilitar”, el sistema inhabilita el contenido. Si se pulsa sobre el botón “Cancelar”, el sistema
                                           cierra la alerta sin inhabilitar el contenido. <br>
                                        <img src="../images/ayudas/estadocontenido.png" alt="Error de registro" width="200px" height="100px"/><br>
                                        <h5>Activar contenido</h5>
                                        Después de cambiar la pestaña para mostrar los contenidos inhabilitados, y en el caso de que existan
                                        efectivamente contenidos inhabilitados; al pulsar con el botón izquierdo del mouse sobre el botón
                                        candado abierto, muestra una alerta de confirmación del cambio de estado. <br>
                                        <img src="../images/ayudas/estadocontenido2.png" alt="Error de registro" width="200px" height="100px"/><br>

                                     </p>
                                    </section>
                                </form>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                    <div class="modal fade bs-example-modal-lg" id="modal7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myLargeModalLabel">Grupo</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>

                          <!-- vertical wizard -->
                          <div class="box box-default">
                                <!-- /.box-header -->
                                <div class="box-body wizard-content">
                                    <form action="#" class="tab-wizard vertical wizard-circle">
                                        <!-- Step 1 -->
                                        <h6>Registrar Grupo</h6>
                                        <section>
                                           <p>
                                            Para crear un nuevo grupo se debe ingresar en el formulario de registro, este campo es obligatorio ya
                                            que está marcado con un asterisco (*) y el sistema no permitirá crear un grupo sin nombre, aula,
                                            integrantes y programación. <br>
                                             <img src="../images/ayudas/registrogrupo.png" alt="Error de registro" width="700px" height="350px"/><br>
                                             Para guardar el grupo, se debe pulsar con el botón izquierdo del mouse sobre el botón “Guardar”,
                                             ubicado en la parte inferior derecha de la ventana del formulario.
                                           </p>
                                        </section>
                                        <!-- Step 2 -->
                                        <h6>Editar grupo</h6>
                                        <section>
                                           <p>
                                            Al terminar de editar los datos del grupo que requieran cambio, se debe pulsar con el botón izquierdo del
                                            mouse sobre el botón “Actualizar”, ubicado en la parte inferior izquierda del formulario. <br>
                                            <img src="../images/ayudas/editargrupo.png" alt="Error de registro" width="700px" height="350px"/><br>
                                            Si no se desea editar ningún dato del grupo, se debe pulsar con el botón izquierdo del mouse en el botón “Cancelar”,
                                            ubicado en la parte inferior derecha del formulario. <br>

                                            Para cerrar la ventana sin hacer cambios en los datos del grupo, se debe pulsar con el botón izquierdo
                                            del mouse sobre la equis (x) ubicada en la parte superior derecha de la ventana. <br>
                                           </p>
                                        </section>
                                        <!-- Step 3 -->
                                        <h6>Listar grupo</h6>
                                        <section>
                                            <p>
                                              Al entrar en la sección de grupos del módulo de Gestión de seguimiento, el sistema muestra la lista de
                                               los grupos registrados en el sistema. <br>
                                             <img src="../images/ayudas/listagrupo.png" alt="Error de registro" width="700px" height="350px"/><br>
                                             Para buscar un grupo dentro de la lista de grupos, se debe ingresar los datos del mismo en la barra
                                             de búsqueda ubicada en la parte superior de la tabla de grupos.
                                            </p>
                                        </section>
                                        <!-- Step 4 -->
                                        <h6>Estado de grupo</h6>
                                        <section>
                                            <p>
                                             <h5>inhabilitar grupo</h5>
                                             Al pulsar con el botón izquierdo del mouse sobre el botón candado, se muestra una alerta de confirmación
                                             para inhabilitar el grupo respectivo a el botón. En caso de ser confirmada la acción en el botón “Inhabilitar”,
                                             el sistema inhabilita el grupo. Si se pulsa sobre el botón “Cancelar”, el sistema cierra la alerta sin inhabilitar el grupo. <br>
                                             <img src="../images/ayudas/estadogrupo.png" alt="Error de registro" width="200px" height="100px"/><br>
                                             <h5>Activar grupo</h5>
                                             <img src="../images/ayudas/estadogrupo2.png" alt="Error de registro" width="200px" height="100px"/><br>
                                             Después de cambiar la pestaña para mostrar los grupos inhabilitados, y en el caso de que existan
                                             efectivamente grupos inhabilitados; al pulsar con el botón izquierdo del mouse sobre el botón
                                             candado abierto, muestra una alerta de confirmación del cambio de estado

                                            </p>
                                        </section>
                                    </form>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

        </section>
        	<!-- jQuery 3 -->

            <script src="../js/jquery.slim.js"></script>

            <!-- popper -->
	<script src="../plantilla/popper/dist/popper.min.js"></script>

	<!-- Bootstrap 4.0-->
	<script src="../plantilla/bootstrap/dist/js/bootstrap.min.js"></script>


	<!-- FastClick -->
	<script src="../plantilla/fastclick/lib/fastclick.js"></script>

	<!-- UltimatePro Admin App -->
	<script src="../js/template.js"></script>

	<!-- UltimatePro Admin for demo purposes -->
	<script src="../js/demo.js"></script>

	<!-- steps  -->
	<script src="../plantilla/jquery-steps-master/build/jquery.steps.js"></script>

	<!-- Sweet-Alert  -->
    <script src="../plantilla/sweetalert/sweetalert.min.js"></script>

    <!-- wizard  -->
    <script src="../js/pages/steps.js"></script>

@endsection
