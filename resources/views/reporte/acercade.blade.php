@extends('layout.index')

@section('top')

<div class="content-header">
<div class="d-flex align-items-center">
<div class="mr-auto">
<h3 class="page-title">Inicio</h3>
<div class="d-inline-block align-items-center">
<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
        <li class="breadcrumb-item" aria-current="page">Acerca de</li>
    </ol>
</nav>
</div>
</div>
</div>
</div>

@endsection

@section('contenido')

<section class="content">
<div class="row">


<!--Box footer!-->
<div class="col-md-6 col-12">
<div class="box">
    <div class="box-header with-border">
    <h4 class="box-title">Problema a solucionar</h4>
    </div>
    <div class="box-body">
    <p>Se requiere desarrollar un aplicativo web para la Casa Cultural Andina que permita la gestión y administración de la información relacionada al servicio principal que esta ofrece: Enseñanza de idiomas a la comunidad.
        Con el fin de garantizar control y manejo de la información, el aplicativo administrará procesos tales como grupos, estudiantes, docentes y contenidos temáticos de éstos.
        </p>
    </div>
</div>
</div>

    <!--Box footer!-->
    <div class="col-md-6 col-12">
    <div class="box">
        <div class="box-header with-border">
        <h4 class="box-title">Informacion sobre el sistema</h4>
        </div>
        <div class="box-body">
        <p>El sistema de información LMS CCA, es un aplicativo web intranet elaborado para Casa Cultural Andina, la cual facilita la gestión de información para sus procesos y movimientos en los módulos de: grupos, estudiantes, matriculas, docentes y contenidos temáticos de éstos.</p>
        </div>
    </div>
    </div>

    <!--Box footer!-->
    <div class="col-md-6 col-12">
        <div class="box">
            <div class="box-header with-border">
            <h4 class="box-title">Casa Cultural Andina</h4>
            </div>
            <div class="box-body">
                <p>La Casa Cultural Andina se ubica en una encantadora edificación del principiso del siglo XX, en un tranquilo barrio de la ciudad de Medellín. Tenemos espacios para clases, lecturas, encuentros y patios para exposiciones y convivencia.</p>
            </div>
        </div>
        </div>


            <!--Box footer!-->
<div class="col-md-6 col-12">
    <div class="box">
        <div class="box-header with-border">
        <h4 class="box-title">Informacion sobre el sistema</h4>
        </div>
        <div class="box-body">
            <p>La Casa Cultural Andina ubicada en Medellín - Antioquia calle 45 E número 72-09 es un espacio creado para ofrecer un ambiente agradable que fomenta el intercambio de conocimiento, capaz de albergar la enseñanza y la práctica de idiomas a través de lecturas, talleres, clases grupales e individuales., </p>
        </div>
    </div>
    </div>

</div>


</section>

@endsection
