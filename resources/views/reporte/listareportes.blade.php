@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Reportes</li>
            </ol>
        </nav>
      </div>
    </div>
  </div>
</div>

@endsection

@section('contenido')

<section class="content">
    <div class="row">


    <!--Box footer!-->
      <div class="col-md-6 col-12">
        <div class="box">
          <div class="box-header with-border">
            <h4 class="box-title">Reporte de todas la matricula</h4>
          </div>
          <div class="box-body">
            <p>Este reporte generara un archivo pdf con todas las matriculas.</p>
          </div>
          <div class="box-footer">
            <a href="{{ route('reporte.pdf') }}"><button class="btn btn-primary">generar PDF</button></a>
          </div>
        </div>
      </div>

          <!--Box footer!-->
          <div class="col-md-6 col-12">
            <div class="box">
              <div class="box-header with-border">
                <h4 class="box-title">Reporte del mes anterior</h4>
              </div>
              <div class="box-body">
                <p>Este reporte generara un archivo pdf con todas las matriculas del mes anterior.</p>
              </div>
              <div class="box-footer">
                <a href="{{ route('reportemes.pdf') }}"><button class="btn btn-primary">generar PDF</button></a>
              </div>
            </div>
          </div>

                    <!--Box footer!-->
                    <div class="col-md-6 col-12">
                      <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">Reporte del mes actual</h4>
                        </div>
                        <div class="box-body">
                          <p>Este reporte generara un archivo pdf con todas las matriculas del mes encurso.</p>
                        </div>
                        <div class="box-footer">
                          <a href="{{ route('reporteactual.pdf') }}"><button class="btn btn-primary">generar PDF</button></a>
                        </div>
                      </div>
                    </div>


    </div>


</section>

@endsection
