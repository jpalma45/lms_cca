
<!--Modal Actualizar-->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="box-title text-info" id="exampleModalCenterTitle"><i class="ti-user mr-15"></i>Actualizar Informacion</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="editarpersona"action="{{ route('persona.update','text') }}" method="POST"  autocomplete="off">
          {{ method_field('patch') }}
          {{ csrf_field() }}
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
            <div class="form-group">
              <input type="hidden" name="idpersona" id="person" value="">
              <label>Primer nombre <span class="text-danger">*</span></label>
              <input name="nombre" id="nombre" type="text" class="form-control" placeholder="Primer Nombre">
              {!!$errors->first('nombre','<span class=error>:message</span>')!!}
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group">
              <label>Segundo nombre</label>
              <input name="snombre" id="snombre" type="text" class="form-control" placeholder="Segundo Nombre">
              {!!$errors->first('snombre','<span class=error>:message</span>')!!}
            </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label >Apellido <span class="text-danger">*</span></label>
                <input name="apellido" id="firsname" type="text" class="form-control" placeholder="Apellido">
                {!!$errors->first('apellido','<span class=error>:message</span>')!!}
              </div>
              </div>
          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                    <label >Cédula  <span class="text-danger">*</span></label>
                    <input name="cedula" id="cdla" type="text" onkeyup="format(this)" onchange="format(this)" minlength="10" maxlength="12" class="form-control" placeholder="Cedula">
                    {!!$errors->first('cedula','<span class=error>:message</span>')!!}
               </div>
             </div>
              <div class="col-md-4">
              <div class="form-group">
                <label >teléfono <span class="text-danger">*</span></label>
                <input name="telefono" id="phone" minlength="9" maxlength="12" type="text" onkeyup="format(this)" onchange="format(this)" class="form-control telefono" maxlength="10" placeholder="Telefono">
                {!!$errors->first('felefono','<span class=error>:message</span>')!!}
              </div>
              </div>
              <div class="col-md-4">
              <div class="form-group">
                <label >Dirección <span class="text-danger">*</span></label>
                <input name="direccion" id="ruta" type="text" class="form-control" placeholder="Direcciòn">
                {!!$errors->first('direccion','<span class=error>:message</span>')!!}
              </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                      <label >Fecha de nacimiento <span class="text-danger">*</span></label>
                      <input name="fecha_nacimiento" id="nacimiento" type="date" class="form-control" placeholder="Fecha de Nacimiento">
                      {!!$errors->first('fecha_nacimiento','<span class=error>:message</span>')!!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label >Email <span class="text-danger">*</span></label>
                      <input name="email" id="email" value="{{ old('email') }}" type="email" class="form-control" placeholder="Email">
                      {!!$errors->first('email','<span class=error>:message</span>')!!}
                  </div>
                </div>
              </div>
        </div>
        <!-- /.box-body -->
        <div class="modal-footer modal-footer-uniform">
            <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Actualizar</button>
        </div> 
      </form>
      </div>
    </div>
  </div>
</div>


<!--AGREGAR-->
  <div data-backdrop="static" data-keyboard="false" class="modal center-modal fade" id="modal-center" tabindex="-1">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Registrar Persona</h4>
              <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <div class="box">
                <!-- /.box-header -->
                
                <form class="form" id="modalpersona" action="{{ route('persona.store') }}" method="POST"  autocomplete="off">
                    {{ csrf_field() }}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-4">
                      <div class="form-group">
                        <label>Primer nombre <span class="text-danger">*</span></label>
                        <input name="nombre" id="" value="{{ old('nombre') }}" type="text" class="form-control" placeholder="Primer Nombre">
                        {!!$errors->first('nombre','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                      <div class="col-md-4">
                      <div class="form-group">
                        <label>Segundo nombre</label>
                        <input type="hidden" name="snombre" value=".">
                        <input name="snombre" id="" value="{{ old('snombre') }}" type="text" class="form-control" placeholder="Segundo Nombre">
                        {!!$errors->first('snombre','<span class=error>:message</span>')!!}
                      </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label >Apellido <span class="text-danger">*</span></label>
                          <input name="apellido" id="" value="{{ old('apellido') }}" type="text" class="form-control" placeholder="Apellido">
                          {!!$errors->first('apellido','<span class=error>:message</span>')!!}
                        </div>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                          <label >Teléfono <span class="text-danger">*</span></label>
                          <input name="telefono" id="" value="{{ old('telefono') }}" minlength="9" maxlength="12" type="text" onkeyup="format(this)" onchange="format(this)" class="form-control" placeholder="Telefono">
                          {!!$errors->first('telefono','<span class=error>:message</span>')!!}
                        </div>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                          <label >Dirección  <span class="text-danger">*</span></label>
                          <input name="direccion" id="" value="{{ old('direccion') }}" type="text" class="form-control" placeholder="Direcciòn">
                          {!!$errors->first('direccion','<span class=error>:message</span>')!!}
                        </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label >Cédula <span class="text-danger">*</span></label>
                            <input name="cedula" id="" value="{{ old('cedula') }}"  type="text" onkeyup="format(this)" onchange="format(this)" minlength="10" maxlength="12" class="form-control" placeholder="Cedula">
                            {!!$errors->first('cedula','<span class=error>:message</span>')!!}
                          </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                <label >Fecha de nacimiento <span class="text-danger">*</span></label>
                                <input name="fecha_nacimiento" id="" value="{{ old('fecha_nacimiento') }}" type="date" max="{{ $date }}" class="form-control fecha" placeholder="Fecha de Nacimiento">
                                {!!$errors->first('fecha_nacimiento','<span class=error>:message</span>')!!}
                              </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label >Email <span class="text-danger">*</span></label>
                              <input name="email" id="" value="{{ old('email') }}" type="email" class="form-control" placeholder="Email">
                              {!!$errors->first('email','<span class=error>:message</span>')!!}
                          </div>
                        </div>
                          <div class="col-md-4" id="especia" style="display: none;">
                            <div class="form-group">
                              <label>especialización </label>
                              <input name="especializacion" id="esp" type="text" class="form-control" placeholder="Especializacion">
                              {!!$errors->first('especializacion','<span class=error>:message</span>')!!}
                            </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <h5>Rol <span class="text-danger">*</span></h5>
                              <fieldset class="controls">
                                <input name="Id_tipopersona" type="radio" id="radio_1" value="1" onchange="javascript:showContent()">
                                <label for="radio_1">Docente</label>
                              </fieldset>
                              <fieldset>
                                <input name="Id_tipopersona" type="radio" id="radio_2" onchange="javascript:hidenContent()" value="2">
                                <label for="radio_2">Estudiante</label>									
                              </fieldset>
                            </div>
                          </div>
                        </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="modal-footer modal-footer-uniform">
                      <button type="button" class="btn btn-danger btn-outline" data-dismiss="modal"><i class="ti-trash"></i> Cancelar</button>
                      <button type="submit" class="btn btn-primary btn-outline float-right"><i class="ti-save-alt"></i> Guardar</button>
                  </div> 
                </form>
                <!-- /.box -->			
          </div>
        </div>
      </div>
