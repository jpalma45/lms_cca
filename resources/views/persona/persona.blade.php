@extends('layout.index')

@section('top')

<div class="content-header">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title">Inicio</h3>
      <div class="d-inline-block align-items-center">
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Persona</li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="right-title">
        <button type="button" class="btn btn-success" data-toggle="tooltip" title="Registrar Persona" data-toggle="modal" data-target="#modal-center"><a class="text-dark"><i class="ti-save" aria-hidden="true"></i></a>
						Registrar Persona
        </button>
    </div>
  </div>
</div>

@endsection

@section('contenido')


<div class="col-12">
    <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">Registros</h4>
      <h6 class="box-subtitle">Informacion Casa Cultural</h6>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home8" role="tab"><span><i class="icon ion-university mr-15"></i>Aprendices</span></a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile8" role="tab"><span><i class="ion-person mr-15"></i>Docentes</span></a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages8" role="tab"><span><i class="ion-email mr-15"></i>Inactivos</span></a> </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content tabcontent-border">
        <div class="tab-pane active" id="home8" role="tabpanel">
          <div class="p-15">

        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped pdf">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Cedula</th>
                  <th>Telefono</th>
                  <th>Direcciòn</th>
                  <th>Fecha de Nacimiento</th>
                  <th class="tamaño" >Operación </th>
                </tr>
              </thead>
            <tbody>

                @foreach ($personas as $cca)
                <tr>
                  <td>{{ $cca->nombre }}&nbsp;{{ $cca->snombre }}&nbsp;{{ $cca->apellido }}</td>
                  <td>{{ $cca->cedula }}</td>
                  <td>{{ $cca->telefono }}</td>
                  <td>{{ $cca->direccion }}</td>
                  <td>{{ $cca->fecha_nacimiento }}</td>
                  <td>
                    <button type="button" class="btn btn-success mb-3"
                    data-mynombre={{ $cca->nombre }}
                    data-mysname={{ $cca->snombre }}
                    data-myapellido={{ $cca->apellido }}
                    data-mycedula={{ $cca->cedula }}
                    data-mytelefono={{ $cca->telefono }}
                    data-mydireccion={{ $cca->direccion }}
                    data-myfecha={{ $cca->fecha_nacimiento }}
                    data-mytipo={{ $cca->Id_tipopersona }}
                    data-myidpersona={{ $cca->idpersona }}
                    data-myemail={{ $cca->email }}
                    data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                    <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                  </button>
                    <form style="display: inline" method="POST" id="int" action="{{ route('persona.destroy',$cca->idpersona)}}">
                        {!! csrf_field() !!}
                        {!! method_field('DELETE') !!}
                        <input type="hidden" name="idpersona" value="{{ $cca->idpersona }}">

                        <button type="button" class="btn btn-warning mb-3 sweetalert" data-toggle="tooltip" data-original-title="Inhabilitar"><a class="text-dark"><i class="ti-lock" aria-hidden="true"></i></a>
                        </button>
                      </form>
                  </td>
                </tr>
                @endforeach

            </tbody>
            <tfoot>
              <tr>
                  <th>Nombre</th>
                  <th>Cedula</th>
                  <th>Telefono</th>
                  <th>Direcciòn</th>
                  <th>Fecha de Nacimiento</th>
                  <th class="tamaño" >Operación </th>
              </tr>
            </tfoot>
            </table>
          </div>
        </div>
        <!-- /.box-body -->


          </div>
        </div>
        <div class="tab-pane" id="profile8" role="tabpanel">
          <div class="p-15">

                   <!-- /.box-header -->
                   <div class="box-body">
                     <div class="table-responsive">
                       <table id="example5" class="table table-bordered table-striped" style="width:100%">
                       <thead>
                         <tr>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Telefono</th>
                            <th>Direcciòn</th>
                            <th>Fecha de Nacimiento</th>
                            <th class="tamaño" >Operación </th>
                         </tr>
                       </thead>
                       <tbody>

                          @foreach ($docentes as $cca)
                          <tr>
                            <td>{{ $cca->nombre }}&nbsp;{{ $cca->snombre }}&nbsp;{{ $cca->apellido }}</td>
                            <td>{{ $cca->cedula }}</td>
                            <td>{{ $cca->telefono }}</td>
                            <td>{{ $cca->direccion }}</td>
                            <td>{{ $cca->fecha_nacimiento }}</td>
                             <td>
                              <button type="button" class="btn btn-success mb-3"
                              data-mynombre={{ $cca->nombre }}
                              data-mysname={{ $cca->snombre }}
                              data-myapellido={{ $cca->apellido }}
                              data-mycedula={{ $cca->cedula }}
                              data-mytelefono={{ $cca->telefono }}
                              data-mydireccion={{ $cca->direccion }}
                              data-myfecha={{ $cca->fecha_nacimiento }}
                              data-mytipo={{ $cca->Id_tipopersona }}
                              data-myidpersona={{ $cca->idpersona }}
                              data-myemail={{ $cca->email }}
                              data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                              <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                            </button>
                              <form style="display: inline" method="POST" action="{{ route('persona.destroy',$cca->idpersona)}}">
                                  {!! csrf_field() !!}
                                  {!! method_field('DELETE') !!}
                                  <input type="hidden" name="idpersona" value="{{ $cca->idpersona }}">

                                  <button type="submit" class="btn btn-warning mb-3" data-toggle="tooltip" data-original-title="Inhabilitar"><a class="text-dark"><i class="ti-lock" aria-hidden="true"></i></a>
                                  </button>
                                </form>
                            </td>
                          </tr>
                          @endforeach


                       </tbody>
                       <tfoot>
                         <tr>
                           <th></th>
                           <th></th>
                           <th></th>
                           <th></th>
                           <th></th>
                           <th></th>
                         </tr>
                       </tfoot>
                     </table>
                     </div>
                   </div>
                   <!-- /.box-body -->

          </div>
        </div>
        <div class="tab-pane" id="messages8" role="tabpanel">
          <div class="p-15">


              <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                  <table id="" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Cedula</th>
                        <th>Telefono</th>
                        <th>Direcciòn</th>
                        <th>Fecha de Nacimiento</th>
                        <th class="tamaño" >Operación </th>
                      </tr>
                    </thead>
                  <tbody>

                      @foreach ($estado as $cca)
                      <tr>
                        <td>{{ $cca->nombre }}&nbsp;{{ $cca->snombre }}&nbsp;{{ $cca->apellido }}</td>
                        <td>{{ $cca->cedula }}</td>
                        <td>{{ $cca->telefono }}</td>
                        <td>{{ $cca->direccion }}</td>
                        <td>{{ $cca->fecha_nacimiento }}</td>
                        <td>
                          <button type="button" class="btn btn-success mb-3"
                          data-mynombre={{ $cca->nombre }}
                          data-myapellido={{ $cca->apellido }}
                          data-mycedula={{ $cca->cedula }}
                          data-mytelefono={{ $cca->telefono }}
                          data-mydireccion={{ $cca->direccion }}
                          data-myfecha={{ $cca->fecha_nacimiento }}
                          data-mysname={{ $cca->snombre }}
                          data-mytipo={{ $cca->Id_tipopersona }}
                          data-myidpersona={{ $cca->idpersona }}
                          data-myemail={{ $cca->email }}
                          data-toggle="modal" data-target="#editar" data-toggle="tooltip" title="Editar">
                          <a class="text-dark" ><i class="ti-pencil" aria-hidden="true"></i></a>
                        </button>
                          <form style="display: inline" method="POST" action="{{ route('persona.activar',$cca->idpersona)}}">
                              {!! csrf_field() !!}
                              <input type="hidden" name="idpersona" value="{{ $cca->idpersona }}">

                              <button type="submit" class="btn btn-info mb-3" data-toggle="tooltip" data-original-title="Activar"><a class="text-dark"><i class="ti-unlock" aria-hidden="true"></i></a>
                              </button>
                            </form>
                        </td>
                      </tr>
                      @endforeach

                  </tbody>
                  <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Cedula</th>
                        <th>Telefono</th>
                        <th>Direcciòn</th>
                        <th>Fecha de Nacimiento</th>
                        <th class="tamaño" >Operación </th>
                    </tr>
                  </tfoot>
                  </table>
                </div>
              </div>
              <!-- /.box-body -->



          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>


  @include('persona.modal')

@endsection

@section('script')

<script>
    $('#editar').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)
    var snombre = button.data('mysname')
    var name = button.data('mynombre')
    var firstname = button.data('myapellido')
    var cedula = button.data('mycedula')
    var telefono = button.data('mytelefono')
    var direccion = button.data('mydireccion')
    var fecha = button.data('myfecha')
    var email = button.data('myemail')
    var tipo = button.data('mytipo')
    var person_id = button.data('myidpersona')


    var modal = $(this)
    modal.find('.modal-body #sname').val(snombre)
    modal.find('.modal-body #nombre').val(name)
    modal.find('.modal-body #firsname').val(firstname)
    modal.find('.modal-body #cdla').val(cedula)
    modal.find('.modal-body #phone').val(telefono)
    modal.find('.modal-body #ruta').val(direccion)
    modal.find('.modal-body #nacimiento').val(fecha)
    modal.find('.modal-body #tipopersona').val(tipo)
    modal.find('.modal-body #email').val(email)
    modal.find('.modal-body #person').val(person_id)


  });
  </script>


  @if($errors->any())
  <script>
  $('#modal-center').modal('show');
  </script>
  @endif

  @if($errors->any())
  <script>
  $('#').modal('show');
  </script>
  @endif
@endsection
