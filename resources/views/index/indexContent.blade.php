@extends('layout.index')

@section('top')

<div class="content-header">
    <div class="d-flex align-items-center">
        <div class="mr-auto w-p50">
            <h3 class="page-title">Inicio</h3>
            <div class="d-inline-block align-items-center">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Control</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
    

<div class="row">	
  <div class="col-xl-4 col-12">
    <!-- small box -->
    <div class="small-box box-inverse bg-img" style="background-image: url(../../images/gallery/thumb/6.jpg);" data-overlay="5">
    <div class="inner">
      <h3>{{ $personas }}</h3>

      <p>personas</p>
    </div>
    <div class="icon text-white">
      <i class="fa fa-database"></i>
    </div>
    <a href="{{ route('persona.index') }}" class="small-box-footer">Mas información<i class="fa fa-arrow-right"></i></a>
    </div>
  </div>
  <!-- /.col -->
  <div class="col-xl-4 col-12">
    <!-- small box -->
    <div class="small-box box-inverse bg-img" style="background-image: url(../../images/gallery/thumb/6.jpg);" data-overlay="5">
    <div class="inner">
      <h3>{{ $matriculas }}</h3>

      <p>Matriculas</p>
    </div>
    <div class="icon text-white">
      <i class="fa fa-graduation-cap"></i>
    </div>
    <a href="{{ route('matricula.index') }}" class="small-box-footer">Mas información<i class="fa fa-arrow-right"></i></a>
    </div>
  </div>
  <!-- /.col -->
  <div class="col-xl-4 col-12">
    <!-- small box -->
    <div class="small-box box-inverse bg-img" style="background-image: url(../../images/gallery/thumb/6.jpg);" data-overlay="5">
    <div class="inner">
      <h3>{{ $grupo }}</h3>

      <p>Grupos</p>
    </div>
    <div class="icon text-white">
      <i class="fa fa-users"></i>
    </div>
    <a href="{{ route('grupo.index') }}" class="small-box-footer">Mas información<i class="fa fa-arrow-right"></i></a>
    </div>
  </div>

  </div>

      <div class="col-md-8 col-20">
          <div class="small-box box-inverse bg-img box" style="background-image: url(../../images/gallery/thumb/6.jpg);" data-overlay="5">
            <div class="box-header with-border">
            <ul class="box-controls pull-right">
              <li><a class="box-btn-slide"  href="#"></a></li>	
            </ul>
            </div>
                  <div class="box bg-transparent no-border no-shadow">	
                    <div class="box-body text-center">
                      <!--timer-->
                      <div class="examples my-10">
                        <div id="countdown" class="row justify-content-md-center text-link"></div>
                      </div>
                      <!--//timer-->
                    </div>
                  </div>
          </div>
          </div>
  
@endsection

@section('index')
<!-- /.col -->
<div class="col-xl-4 col-12">
  <div class="info-box">
    <span class="info-box-icon bg-danger rounded"><i class="ion ion-stats-bars"></i></span>

    <div class="info-box-content text-right">
      <span class="info-box-number">$5,354</span>
      <span class="info-box-text">TAX DEDUCATION</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-xl-4 col-12">
  <div class="info-box">
    <span class="info-box-icon bg-success rounded"><i class="ion ion-thumbsup"></i></span>

    <div class="info-box-content text-right">
      <span class="info-box-number">$1,642</span>
      <span class="info-box-text">REVENUE STATUS</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

@endsection